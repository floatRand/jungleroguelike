﻿
using System;

namespace Roguelike_Project
{
	/** Based on http://www.roguebasin.com/index.php?title=LOS_using_strict_definition, thanks for anyone who made it!**/
	
	public enum FOVResult{ Seen, NotSeen, Memoried }
	
	public static class LOS{
public static void FOV(int x, int y, int radius, Map map)
{
	FOVResult lightedSquare = FOVResult.NotSeen;
	map.CleanItemFovs();
    for (int i = -13; i <= 13; i++){ //iterate out of map bounds as well
    	for (int j = -13; j <= 13; j++){
			
			if(x+i>=0 && x+i<map.shadowMap.values.GetLength(0) &&  y+j>=0 && y+j<map.shadowMap.values.GetLength(1) ) map.shadowMap.values[x+i,y+j] = 0;
			 if(i+x>=0 && i+x<map.mapdata.GetLength(0) &&  j+y>=0 && j+y<map.mapdata.GetLength(1) ) map.mapdata[i+x,j+y].seen = false; 
			
			if( i>=-(int)radius && i <= radius && j>=-(int)radius && j<radius ){
			  lightedSquare = Calculate(x, y, i, j, map);

			   	if(i+x>=0 && i+x<map.mapdata.GetLength(0) &&  j+y>=0 && j+y<map.mapdata.GetLength(1) ){
			  	if(map.mapdata[i+x,j+y].tileMemory.memorized){ map.shadowMap.values[i+x,j+y] = 255; } // if recalled, check anyways
			  	}
			  
			  if(lightedSquare == FOVResult.Seen){ 
			  	if(i+x>=0 && i+x<map.mapdata.GetLength(0) &&  j+y>=0 && j+y<map.mapdata.GetLength(1) ){
			  	map.mapdata[i+x,j+y].tileMemory.SetMemory( map.mapdata[i+x,j+y].glyph ); // seen tile;
			  	map.mapdata[i+x,j+y].seen = true; 
			    map.shadowMap.values[i+x,j+y] = 255;
			  	if(map.IsCreatureOnTile(x+i,y+j)) map.GetCreatureOnTile(x+i,y+j).inFOV = true; // creature is in FOV
			  	if(map.IsItemOnTile(x+i,y+j)) foreach(ItemBase elm in map.GetItemsOnTile(x+i,y+j) ){ elm.inFov = true; }
			  	}
			  }  
			}
			else{
			
			   	if(i+x>=0 && i+x<map.mapdata.GetLength(0) &&  j+y>=0 && j+y<map.mapdata.GetLength(1) ){
			  	if(map.mapdata[i+x,j+y].tileMemory.memorized){ map.shadowMap.values[i+x,j+y] = 255; } // if recalled, check anyways
			  	}
			
			} // OUTSIDE OF THE FOV, VISIBLE IN SCREEN!!
			
			  
		}
    }
                
}

/* Los calculation */
public static FOVResult Calculate(int x0, int y0, int x1, int y1, Map map) // true if returns lighted square, else returns false
{
   	 int sx, sy, xnext, ynext, dx, dy;
   	 double denom;
   	 x1 = x0+x1; y1 = y0+y1;
   	 dx = x1 - x0;
   	 dy = y1 - y0;
   	 if (x0 < x1) sx = 1; else sx = -1;
   	 if (y0 < y1) sy = 1; else sy = -1;
 	   // sx and sy are switches that enable us to compute the LOS in a single quarter of x/y plan
   	 xnext = x0; ynext = y0;
   	 denom = Math.Sqrt(dx * dx + dy * dy);
   	 while (xnext != x1 || ynext != y1)
   	 {
        // check map bounds here if needed
        if(xnext>=0 && xnext<map.shadowMap.values.GetLength(0) &&  ynext>=0 && ynext<map.shadowMap.values.GetLength(1) ){
        if (!map.mapdata[xnext,ynext].transparent) // or any equivalent
        {
            return FOVResult.Memoried; // here return true because otherwise funny things happen at bounds
        }
        }
        // Line-to-point distance formula < 0.5
        if(Math.Abs(dy * (xnext - x0 + sx) - dx * (ynext - y0)) / denom < 0.5f) xnext += sx;
        else if(Math.Abs(dy * (xnext - x0) - dx * (ynext - y0 + sy)) / denom < 0.5f) ynext += sy;
        else { xnext += sx; ynext += sy; }
    }
   		 if(x1>=0 && x1<map.shadowMap.values.GetLength(0) &&  y1>=0 && y1<map.shadowMap.values.GetLength(1) ){ 
   	 	 return FOVResult.Seen;
   		 }
   	 return FOVResult.NotSeen;
}// END OF CALCULATE


}
}
