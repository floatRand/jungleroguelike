﻿/*
 * Created by SharpDevelop.
 * User: Saska
 * Date: 4.4.2015
 * Time: 1:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using SFML.System;
using SFML.Graphics;
using SFML.Window;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of core.
	/// </summary>
	public class Game
	{
		public bool Running;
		public bool minimapUp;
		public Map theMap, currentMap;
		public RenderWindow window;
		public GlyphRenderer mainGlyphRenderer;
		public GlyphRenderer GUIGlyphRenderer;
		public Player player; public Camera mainCamera;
		public Random random;
		public List<CreatureBase> creaturesInFOVList;
		public NightEffect rainEff;
		public int playerFOVlimit;
		public char inventorySelection; public int inventoryPage;
		public int CursorModeX, CursorModeY; public CreatureBase cursoredCreature;
		public SearchParam srParm;
		public bool gameChanged, initialPress;
		public Overworld JungleWorld; 
		
		public Game()
		{
			
			window = new RenderWindow(new VideoMode(560,420), "Roguelike project");
			window.SetVerticalSyncEnabled(false);
			window.SetFramerateLimit(60);
			gameManager.Instance.window = window;
			Running=true;
			theMap = new Map(128,128);
			
			mainCamera = new Camera();
			mainGlyphRenderer = new GlyphRenderer(27,27, mainCamera);
			GUIGlyphRenderer = new GlyphRenderer(56,45, new Camera());
			gameManager.Instance.guiRend = GUIGlyphRenderer;
			gameManager.Instance.mainRend = mainGlyphRenderer;
			
			random = new Random();
			
			rainEff = new NightEffect();
			creaturesInFOVList = new List<CreatureBase>();
			
			gameManager.Instance.mainCam = mainCamera;
			gameManager.Instance.time.Reset();
			gameManager.Instance.currentMap = theMap;
			currentMap = theMap;
			player = new Player();
			playerFOVlimit = 13;
			player.x = 15; player.y = 15;
			gameManager.Instance.player = player;
			window.TextEntered += new EventHandler<TextEventArgs>(Input.EnteredText);
			minimapUp = false;
			inventorySelection = ' '; inventoryPage = 0;
			srParm = new SearchParam(player.x,player.y,0,0,currentMap);
			
			JungleWorld = new Overworld();
		}
		
		
		
		
		public void Run(){ // where the itself game is run
			
			while(!player.IsSquarePassable(player.x,player.y,gameManager.Instance.currentMap))
			{
				player.x = random.Next(3,30);
				player.y = random.Next(3,30);
				
			}
			srParm.start = new int[]{player.x,player.y};
			for(int i=0;i<4;i++){
			currentMap.creatures.Add(new MONS_DeathHornet(player.x,player.y));
			}
			for(int i=0;i<6;i++){
				currentMap.items.Add(new ItemBase(player.x+RNG.GetValue(-4,4),player.y+RNG.GetValue(-4,4)));
			}
			
			RNG.RandomDistributionTest(10000,3);
			LOS.FOV(player.x,player.y, playerFOVlimit,currentMap);
			while(Running){
				initialPress = false;
				Input.ClearKeys();
				window.DispatchEvents();	

			
				if(gameManager.Instance.menuState==MenuState.none){
				if(Input.IsKeyPressed('8')){ player.HandleMovement(0,-1); }
				if(Input.IsKeyPressed('2')){ player.HandleMovement(0,1); }
				if(Input.IsKeyPressed('4')){ player.HandleMovement(-1,0); }
				if(Input.IsKeyPressed('6')){ player.HandleMovement(1,0); }

				if(Input.IsKeyPressed('7')){ player.HandleMovement(-1,-1); }
				if(Input.IsKeyPressed('9')){ player.HandleMovement(1,-1); }
				if(Input.IsKeyPressed('1')){ player.HandleMovement(-1,1); }
				if(Input.IsKeyPressed('3')){ player.HandleMovement(1,1); }
				if(Input.IsKeyPressed('5') || Input.IsKeyPressed('.')){
					player.SetAction( new turnAction( delegate{},  10,0) ); }
				if(Input.IsKeyPressed('5') || Input.IsKeyPressed(':')){
					player.SetAction( new turnAction( delegate{},  1,0) ); } // minwait
								
				if(Input.IsKeyPressed(',')){
					if(currentMap.IsItemOnTile(player.x,player.y)) currentMap.items.PickupItem(currentMap.GetItemOnTile(player.x,player.y),player.inventory);
				}
				
				if(Input.IsKeyPressed('H')){ 
					CreatureBase testtargetPos;
					
					testtargetPos = Prompts.creatureTargeter( currentMap, false );
					
					if(Prompts.BoolPrompt("Another stupid prompt", ColourName.Red)) 
					MSGH.Print("Alright, you picked yes.");
					else{ MSGH.Print("No? Really?"); }
					
					currentMap.clouds.Add( new GasCloud(
						currentMap,testtargetPos.x,testtargetPos.y,22500,new GAS_Test() ) );
				               Console.WriteLine("Gas placed");
				}
				
				if(Input.IsKeyPressed('W')){ 
					GTime.Get().TURN += 50000000;
				}

				}

				if(Keyboard.IsKeyPressed(Keyboard.Key.Escape)){ gameManager.Instance.menuState =MenuState.none; }
				
				if(gameManager.Instance.menuState==MenuState.none){
				if(Input.IsKeyPressed('a')){ gameManager.Instance.menuState =MenuState.AbilityWindow;}
				if(Input.IsKeyPressed('m')){ gameManager.Instance.menuState =MenuState.Minimap;}
				if(Input.IsKeyPressed('M')){ gameManager.Instance.menuState =MenuState.Overworld;}
				if(Input.IsKeyPressed('x')){ gameManager.Instance.menuState =MenuState.Examine; CursorModeX = player.x; CursorModeY = player.y;}
				if(Input.IsKeyPressed('o')){ gameManager.Instance.menuState =MenuState.Observe;}
				if(Input.IsKeyPressed('i')){ gameManager.Instance.menuState =MenuState.Inventory;}
				if(Input.IsKeyPressed('C')){ gameManager.Instance.menuState =MenuState.CharView;}
				if(Input.IsKeyPressed('w')){ gameManager.Instance.menuState =MenuState.wearItem; initialPress = true;}
				if(Input.IsKeyPressed('T')){ gameManager.Instance.menuState =MenuState.unequipItem; initialPress = true;}
				if(Input.IsKeyPressed('d')){ gameManager.Instance.menuState =MenuState.dropItem; initialPress = true;}
				}
				
				
				//DRAWSTUFF				
				window.Clear(Color.Black);
				GUIGlyphRenderer.ResetGlyphMap();
				mainGlyphRenderer.ResetGlyphMap(true);
				
				
				gameManager.Instance.playerX = player.x; gameManager.Instance.playerY = player.y;
				mainCamera.x = player.x; mainCamera.y = player.y;
				
				player.PlayerDamageColours();
				currentMap.ExtractGlyphMap();
				
				mainGlyphRenderer.posX = 1; mainGlyphRenderer.posY = 1;

				LOS.FOV(player.x,player.y, playerFOVlimit,currentMap);
				mainGlyphRenderer.OverlayLimited(currentMap.glyphMap,0,0,mainCamera.x-13,mainCamera.y-13,27,27,'ö');
				currentMap.SetMemorizedFeatures(mainGlyphRenderer.glyphMap, mainCamera.x-13,mainCamera.y-13 );
				
				creaturesInFOVList.Clear(); 
				creaturesInFOVList.AddRange(currentMap.creatures.FindAll(element => element.inFOV));
				
								if(gameManager.Instance.tripMode){
											int tempx = 0; int tempy=0;
											for(int i=0;i<creaturesInFOVList.Count;i++){
											tempx = creaturesInFOVList[i].x; tempy = creaturesInFOVList[i].y; 
												creaturesInFOVList[i] = null; 
												creaturesInFOVList[i] = new MONS_RandomHallucination(tempx,tempy);

											}
										mainGlyphRenderer.effect = EffectFlag.LSD;
										}
				
						currentMap.DrawItems(mainGlyphRenderer);
						foreach( CreatureBase creature in creaturesInFOVList){creature.Draw(mainGlyphRenderer);}
						currentMap.DrawClouds(mainGlyphRenderer);
						player.Draw(mainGlyphRenderer);
						mainGlyphRenderer.effect = EffectFlag.none;
		
						playerFOVlimit = 13-(int)(GTime.Get().GetDayDarkness()*9);
				
				//PFtest
				/**
				srParm.end = new int[]{player.x,player.y};
				srParm.map = currentMap;
				List<PFNode> testFindLoc = Pathfinding.FindPath(srParm);
				foreach(PFNode loc in testFindLoc){ 
					mainGlyphRenderer.InsertGlyph(loc.x,loc.y,new Glyph('*'),true); 
				}
				**/
				
				//CURSORMODE
				if(gameManager.Instance.menuState == MenuState.Examine)
				{
					
						if(Input.IsKeyPressed('8')){ CursorModeY--; }
						if(Input.IsKeyPressed('2')){ CursorModeY++; }
						if(Input.IsKeyPressed('4')){ CursorModeX--; }
						if(Input.IsKeyPressed('6')){ CursorModeX++; }

						if(Input.IsKeyPressed('7')){ CursorModeX--; CursorModeY--; }
						if(Input.IsKeyPressed('9')){ CursorModeX++; CursorModeY--; }
						if(Input.IsKeyPressed('1')){ CursorModeX--; CursorModeY++;  }
						if(Input.IsKeyPressed('3')){ CursorModeX++; CursorModeY++;  }
						
						if(CursorModeX > player.x+13) CursorModeX = player.x+13;
						if(CursorModeX < player.x-11) CursorModeX = player.x-11;
						if(CursorModeY > player.y+13) CursorModeY = player.y+13;
						if(CursorModeY < player.y-11) CursorModeY = player.y-11;
					
				}
				//ENDCURSORMODE
				if(mainGlyphRenderer.effect!=EffectFlag.none){ mainGlyphRenderer.ApplyEffect(); }
				mainGlyphRenderer.ApplyPartialShadowMap(currentMap.shadowMap,mainCamera.x-13,mainCamera.y-13,27,27);	
				rainEff.UpdateandSetEffect(mainGlyphRenderer);
				mainGlyphRenderer.UpdateArray();
				
				
				
				window.Draw(mainGlyphRenderer.asciiRenderer);
				//viewport around
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,27,27, true);
	
				
				// UI
				GUIStuff.GenerateMSGBox(GUIGlyphRenderer);
				if(gameManager.Instance.menuState == MenuState.Examine) 
						GUIStuff.GenerateCursorViewBox(GUIGlyphRenderer, currentMap, CursorModeX, CursorModeY);
				
				GUIStuff.GeneratePlayerInfo(GUIGlyphRenderer,player);
				GUIStuff.DisplayDurations(GUIGlyphRenderer,player);
				if(player.recentTarget!=null) GUIStuff.GenerateFOVEnemyList(GUIGlyphRenderer,creaturesInFOVList,true);
				else GUIStuff.GenerateFOVEnemyList(GUIGlyphRenderer,creaturesInFOVList,false);

				
				
				if(gameManager.Instance.menuState == MenuState.Minimap){
				GUIGlyphRenderer.OverlayLimited(currentMap.GenerateMiniMap(3),1,1,0,0,32,32,'ö');
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,32,32, true);
				}
				
				if(gameManager.Instance.menuState == MenuState.Overworld){
				JungleWorld.ExtractGlyphMap();
						if(Input.IsKeyPressed('8')){ JungleWorld.playerPosY-=2; }
						if(Input.IsKeyPressed('2')){ JungleWorld.playerPosY+=2; }
						if(Input.IsKeyPressed('4')){ JungleWorld.playerPosX-=2; }
						if(Input.IsKeyPressed('6')){ JungleWorld.playerPosX+=2; }
						
						if(Input.IsKeyPressed('7')){ JungleWorld.playerPosX-=2; JungleWorld.playerPosY-=2; }
						if(Input.IsKeyPressed('9')){ JungleWorld.playerPosX+=2; JungleWorld.playerPosY-=2; }
						if(Input.IsKeyPressed('1')){ JungleWorld.playerPosX-=2; JungleWorld.playerPosY+=2;  }
						if(Input.IsKeyPressed('3')){ JungleWorld.playerPosX+=2; JungleWorld.playerPosY+=2;  }
						
				
				GUIGlyphRenderer.OverlayLimited(JungleWorld.glyphMap,1,1,JungleWorld.playerPosX+25,JungleWorld.playerPosY+25,40,40,'ö');
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,40,40, true);
				}
				
				if(gameManager.Instance.menuState == MenuState.Inventory){
					if(Input.IsAnyKeyPressed()) inventorySelection = Input.GetAKey();
					GUIStuff.GenerateInventoryWindow(GUIGlyphRenderer,player,inventorySelection);
				}
				
				if(gameManager.Instance.menuState == MenuState.Observe){
					GUIStuff.GenerateObserveWindow(GUIGlyphRenderer,creaturesInFOVList, currentMap.items.FindAll(elm => elm.inFov));
				}
				if(gameManager.Instance.menuState == MenuState.CharView){
					GUIStuff.GenerateCharView(GUIGlyphRenderer,player);
				}
				if(gameManager.Instance.menuState == MenuState.CharView){
					GUIStuff.GenerateCharView(GUIGlyphRenderer,player);
				}		
				if(gameManager.Instance.menuState == MenuState.AbilityWindow){
					GUIStuff.GenerateAbilityWindow(GUIGlyphRenderer,player);
				}		
				if(gameManager.Instance.menuState == MenuState.wearItem){
					GUIStuff.GenerateWearItemWindow(GUIGlyphRenderer,player,'a', initialPress);
				}			
				if(gameManager.Instance.menuState == MenuState.unequipItem){
					GUIStuff.GenerateUnequipItemWindow(GUIGlyphRenderer,player,'a', initialPress);
				}			
				if(gameManager.Instance.menuState == MenuState.dropItem){
					GUIStuff.GenerateDropItemWindow(GUIGlyphRenderer,player,'a', initialPress);
				}										
				
				GUIGlyphRenderer.UpdateArray();
				window.Draw(GUIGlyphRenderer.asciiRenderer);

				gameChanged = false;
				if(!player.IsFreeToAct())
				{
					while(!player.IsFreeToAct())
					{
									if(MiscMethod.IsKeyTurn()) currentMap.UpdateClouds();
									player.Update();		
									foreach( CreatureBase creature in CreatureBase.CurrentCreatureList()){creature.inFOV = false; creature.Update();}
									gameManager.Instance.playerX = player.x; gameManager.Instance.playerY = player.y;
									mainCamera.x = player.x; mainCamera.y = player.y;
									currentMap.shadowMap.ClearLights();
									
									LOS.FOV(player.x,player.y, playerFOVlimit,currentMap);
									creaturesInFOVList.Clear(); 
									creaturesInFOVList.AddRange(currentMap.creatures.FindAll(element => element.inFOV));
	
						gameManager.Instance.ActionQueue.ProcessQueue();
						GTime.Get().Increment();
						LOS.FOV(player.x,player.y, playerFOVlimit,currentMap);
						gameChanged = true;
					}
				}
				
				
				window.Display();
				
			}
		
		
		}
		
		static void OnClosed(object sender, EventArgs e)
        {
            Window window = (Window)sender;
            window.Close();
        }
		
	
		
		
	}
}
