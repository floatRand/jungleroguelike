﻿
using System;
using SFML.System;
using SFML.Graphics;

namespace Roguelike_Project
{
	public enum EffectFlag{ none, LSD }
	/// <summary>
	/// Description of GlyphRenderer.
	/// </summary>
	public class GlyphRenderer
	{
		
		public Glyph[,] glyphMap;
		public uint sizeX, sizeY;
		public int posX, posY;
		public int offSetX, offsetY;
		public string theTileSetPath;
		public Camera camera;
		public Texture tileset;
		public EffectFlag effect;
		public AsciiVertexArray asciiRenderer;

		/**################################################################################################**/
		public GlyphRenderer(uint w, uint h, Camera cam, int x0 = 0, int y0=0)
		{	// x and y are in tile coordinates to maintain the consistency
			sizeX = w; sizeY = h;
			posX = x0; posY = y0;
			glyphMap = new Glyph[sizeX,sizeY];
			theTileSetPath = "gfx/ascii.png";
			tileset = new Texture(@theTileSetPath);
			asciiRenderer = new AsciiVertexArray(tileset);
			camera = cam; effect = EffectFlag.none;
		}
		
		/**################################################################################################**/
		public void ResetGlyphMap(bool setBlack = false){
			glyphMap = new Glyph[sizeX,sizeY];
			for(uint i=0;i<glyphMap.GetLength(0);i++){
				for(uint j=0;j<glyphMap.GetLength(1);j++){
					if(!setBlack) glyphMap[i,j] = new Glyph();
					else  glyphMap[i,j] = new Glyph(0,' ',new Colour(0,0,0,255),new Colour(0,0,0,255));
				}
			}
		}
		/**################################################################################################**/
		public void SetGlyphMap(Glyph[,] shit){ 
			ResetGlyphMap();
			if(shit!=null){
				glyphMap = shit;
			}
		}
		/**################################################################################################**/
		public void UpdateArray(){ asciiRenderer.UpdateFunArray(glyphMap, posX, posY, effect); }
		/**################################################################################################**/
		public bool InsertGlyph(int x, int y, Glyph glyph, bool applyCameraTranform = false)
		{
			if(applyCameraTranform){ x = x - gameManager.Instance.mainCam.x + 13;  y = y - gameManager.Instance.mainCam.y + 13;} 
			if(x>=0 && x<glyphMap.GetLength(0) && y>=0 && y<glyphMap.GetLength(1)) // make sure it doesn't go out of array
			{
				Colour oldGlyphBG = glyphMap[x,y].bgColor;
				glyphMap[x,y] = glyph; 
				if(glyph.bgColor.alpha < 155) glyphMap[x,y].bgColor = oldGlyphBG;
				return true;
			}
			return false; // couldn't be placed
		}
		/**################################################################################################**/
		public void Overlay(Array ay, int x, int y, object mask = null)
		{
			glyphMap.Overlay2DArray( ay, x, y, mask );
		}
		/**################################################################################################**/
		public void OverlayLimited(Array ayy, int x, int y, int x2, int y2, int w, int h, object mask = null) // for map-pasting
		{
			// x,y = where it will be placed in the overall glyphmap
			// x2,y2 = where to start the copy from
			// w,h = width/height of the block
			Array lmao = Array.CreateInstance( ayy.GetValue(new int[]{0,0}).GetType(), w, h );
			for(int i=0; i<w; i++){
				for(int j=0; j<w; j++){
					if(x2+i>0 && x2+i<ayy.GetLength(0) && y2+j>0 && y2+j< ayy.GetLength(1))
						lmao.SetValue(ayy.GetValue(new int[]{x2+i,y2+j}), new int[]{i,j});
				}
			}
			glyphMap.Overlay2DArray( lmao, x, y, mask );
		}
		/**################################################################################################**/
		public void ApplyShadowMap(ShadowMap shadowMap) // ALWAYS APPLIED TO THE LEFT CORNER OF RENDER-FIELD!!!!!!!!
		{
			for(uint i=0;i<glyphMap.GetLength(0);i++){
				for(uint j=0;j<glyphMap.GetLength(1);j++){
					if(j<shadowMap.values.GetLength(1) && i<shadowMap.values.GetLength(0)){
					if(shadowMap.values[i,j]>50 && shadowMap.values[i,j]<205){
					glyphMap[i,j].charColor = new Colour(
							(byte)(glyphMap[i,j].charColor.red/2),
							(byte)(glyphMap[i,j].charColor.blue/2),
							(byte)(glyphMap[i,j].charColor.green/2)
					                                    );
					glyphMap[i,j].bgColor = new Colour(
							(byte)(glyphMap[i,j].bgColor.red/2),
							(byte)(glyphMap[i,j].bgColor.blue/2),
							(byte)(glyphMap[i,j].bgColor.green/2)
					                                    );
					
					}
					else if( shadowMap.values[i,j]<=50 )
					{
					glyphMap[i,j].charColor = new Colour(
														 0,
					                                     0,
					                                     0
					                                    );
					glyphMap[i,j].bgColor = new Colour(
														 0,
					                                     0,
					                                     0
					                                    );
					}
					
					}
				}
			}
			return;
		}
		/**################################################################################################**/
		public void ApplyPartialShadowMap(ShadowMap shadowMap, int x2, int y2, int w, int h)
		{
			for(uint i=0;i<w;i++){
				for(uint j=0;j<h;j++){
					if((x2+i)<shadowMap.values.GetLength(0) && (y2+j)<shadowMap.values.GetLength(0)
					   && i<glyphMap.GetLength(0) && j<glyphMap.GetLength(1)
					   && x2+i>0 && y2+j>0){
					if(shadowMap.values[x2+i,y2+j]>50 && shadowMap.values[x2+i,y2+j]<205){
					glyphMap[i,j].charColor = new Colour(
							(byte)(glyphMap[i,j].charColor.red/2),
							(byte)(glyphMap[i,j].charColor.blue/2),
							(byte)(glyphMap[i,j].charColor.green/2)
					                                    );
					glyphMap[i,j].bgColor = new Colour(
							(byte)(glyphMap[i,j].bgColor.red/2),
							(byte)(glyphMap[i,j].bgColor.blue/2),
							(byte)(glyphMap[i,j].bgColor.green/2)
					                                    );
					
					}
					else if( shadowMap.values[x2+i,y2+j]<=50 )
					{
					glyphMap[i,j].charColor = new Colour(
														 0,
					                                     0,
					                                     0
					                                    );
					glyphMap[i,j].bgColor = new Colour(
														 0,
					                                     0,
					                                     0
					                                    );
					}
					
					}
				}
			}
			return;

		}
		/**################################################################################################**/
		public void ApplyEffect()
		{
			if(effect == EffectFlag.LSD){
			for(uint i=0;i<glyphMap.GetLength(0);i++){
				for(uint j=0;j<glyphMap.GetLength(1);j++){
						glyphMap[i,j].charColor = new Colour( (byte)(RNG.GetValue(25,255)),
						                                      (byte)(RNG.GetValue(25,255)),
						                                      (byte)(RNG.GetValue(25,255)),
						                                      glyphMap[i,j].charColor.alpha);
				}
			}}
			return;
		}
		
		
	}

	/**################################################################################################**/
	
	public class AsciiVertexArray : Drawable{
	
		public VertexArray vArray = new VertexArray(PrimitiveType.Quads);
		VertexArray bg = new VertexArray(PrimitiveType.Quads);
		public Texture Sheet;
		public bool visible;
		public AsciiVertexArray(Texture sheet){
			Sheet = sheet; vArray = new VertexArray(PrimitiveType.Quads);
			visible = true;
			vArray = new VertexArray(PrimitiveType.Quads);
			bg = new VertexArray(PrimitiveType.Quads);
		}
		
		public void UpdateFunArray(Glyph[,] glyphMap, int x0, int y0, EffectFlag effect = EffectFlag.none){
			vArray.Clear();
			bg.Clear();
			int totalGlyphs = glyphMap.GetLength(0)*glyphMap.GetLength(1);
			vArray.Resize( 4 * (uint)totalGlyphs ); bg.Resize(4 * (uint)totalGlyphs);
			Color bgcolor;
			Color charcolor;
			int size = gameManager.Instance.glyphSize;
			uint cnt = 0;

			for(uint i=0;i<glyphMap.GetLength(0);i++){
				for(uint j=0;j<glyphMap.GetLength(1);j++){
				
					
					charcolor = Colour.ConvertToSFMLColour(glyphMap[i,j].charColor);
					bgcolor = Colour.ConvertToSFMLColour(glyphMap[i,j].bgColor);
					
					Vector2f zr = new Vector2f( 0,0 );
					Vector2f pos = new Vector2f( x0 * size , y0 * size );
					Vector2f asciiPos = new Vector2f(16,176);
					asciiPos = asciiPicker.getAsciiCoords(glyphMap[i,j].character);
					
					zr = new Vector2f( -size/2, -size/2 );
						bg[cnt] = new Vertex( pos + new Vector2f(i,j)*size + zr, bgcolor, new Vector2f( 0,  0));
					zr = new Vector2f( -size/2, size/2 ); 
				   		bg[cnt+1] = new Vertex( pos + new Vector2f(i,j)*size + zr, bgcolor, new Vector2f( 0,  10));
					zr = new Vector2f( size/2, size/2 ); 
				    	bg[cnt+2] = new Vertex( pos + new Vector2f(i,j)*size + zr, bgcolor, new Vector2f( 10,  10));
					zr = new Vector2f( size/2, -size/2 );
						bg[cnt+3] = new Vertex( pos + new Vector2f(i,j)*size + zr, bgcolor, new Vector2f( 10,  0));
					
					zr = new Vector2f( -size/2, -size/2 );
						vArray[cnt] = new Vertex( pos + new Vector2f(i,j)*size + zr, charcolor, asciiPos + new Vector2f(0,0));
					zr = new Vector2f( -size/2, size/2 ); 
				   		vArray[cnt+1] = new Vertex( pos + new Vector2f(i,j)*size + zr, charcolor, asciiPos + new Vector2f(0,size));
					zr = new Vector2f( size/2, size/2 ); 
				    	vArray[cnt+2] = new Vertex( pos + new Vector2f(i,j)*size + zr, charcolor, asciiPos + new Vector2f(size,size));
					zr = new Vector2f( size/2, -size/2 );
						vArray[cnt+3] = new Vertex( pos + new Vector2f(i,j)*size + zr, charcolor, asciiPos + new Vector2f(size,0));
						
					cnt = cnt + 4;	
					}
				
			}
		
			
		}
		
		
		public virtual void Draw(RenderTarget target, RenderStates states){
			states.Texture = Sheet;
			states.BlendMode = BlendMode.Alpha;
			//states.Transform.Scale(0.85f,0.85f);
			RenderStates states2 = new RenderStates();
			states2.Transform = states.Transform;
			states2.BlendMode = BlendMode.Alpha;
			target.Draw( bg, states2 );
			target.Draw( vArray, states );
		}
	}
}
