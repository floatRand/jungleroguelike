﻿
using System;
using System.Collections.Generic;
using SFML.System;
using SFML.Graphics;
using SFML.Window;

namespace Roguelike_Project
{
	public enum MenuState{none, Examine, Minimap, Observe, Inventory, CharView, AbilityWindow, Overworld, wearItem, unequipItem, dropItem};
	
	public sealed class gameManager
	{
		private static readonly gameManager instance = new gameManager();
		public readonly int glyphSize = 10;
		public int playerX, playerY;
        public GTime time;
		public Camera mainCam;
		public MessageConsole msgConsole;
		public ActionQueueClass ActionQueue;
		public Map currentMap;
		public Random random;
		public Player player;
		public InputManager inputManager;
		public MenuState menuState;
		public bool tripMode = false;
		public float turntoTimeConversionValue = 0.00008f;
		public RenderWindow window;
		
		public GlyphRenderer mainRend, guiRend;
		
		public static gameManager Instance {
			get {
				return instance;
			}
		}
		
		private gameManager()
		{
			msgConsole = new MessageConsole();
			ActionQueue = new ActionQueueClass();
			random = new Random();
			inputManager = new InputManager();
			menuState = MenuState.none;
			time = new GTime();
		}
	}
	
	
	
	
}
