﻿
using System;

namespace Roguelike_Project
{
	//Silly visual thing!
	
	public class GlyphEffector
	{
		public int frame;
		public GlyphEffector(){frame = 0;}
		public virtual void UpdateandSetEffect(GlyphRenderer glp){}
	}
	
	
	public class NightEffect : GlyphEffector
	{
		public NightEffect() : base(){}
		public override void UpdateandSetEffect(GlyphRenderer glp)
		{
			frame++;
			int darkener = Colour.lT255((int)(GTime.Get().GetDayDarkness()*160));
			Colour blueOverlay = new Colour((byte)(255-darkener),(byte)(255-darkener),255);
			for(int i=0; i<glp.glyphMap.GetLength(0);i++){
			for(int j=0; j<glp.glyphMap.GetLength(1);j++){
					if(darkener>2){
					glp.glyphMap[i,j].bgColor = Colour.Multiply( glp.glyphMap[i,j].bgColor, blueOverlay  );
					glp.glyphMap[i,j].charColor = Colour.Multiply( glp.glyphMap[i,j].charColor, blueOverlay  );
					}
				}
				}
		}
	}
	
	
	
}
