﻿
using System;

namespace Roguelike_Project
{

	public static class ArrayCreation
	{
		/**#################################################################################################**/
 		/**############################################ CIRCLE  ############################################**/
		/**#################################################################################################**/
		public static Array createCircle( Object obj, int radius, Object emptyObj, bool floodFill = true, Object floodFillObj = null )
		{
			if(radius<0) radius = 0;
			/** Creates a centered circle ( [radius*2,radius*2], origin at radius,radius )**/
			Array res = Array.CreateInstance( obj.GetType(), radius*2+2, radius*2+2 );
			int x = radius; int y = 0;
			int x0 = radius+1; int y0 = radius+1;
			int radErr = 1 - x;
			
			for(int i = 0; i < res.GetLength(0); i++){
				for(int j = 0; j < res.GetLength(1); j++){
						res.SetValue(emptyObj, new int[]{i,j}); // initialize array
				}
			}
			
			while(x >= y){
				res.SetValue(obj, new int[]{ x + x0, y + y0});
				res.SetValue(obj, new int[]{ y + x0, x + y0});
				res.SetValue(obj, new int[]{-x + x0, y + y0});
				res.SetValue(obj, new int[]{-y + x0, x + y0});
				res.SetValue(obj, new int[]{-x + x0,-y + y0});
				res.SetValue(obj, new int[]{-y + x0,-x + y0});
				res.SetValue(obj, new int[]{ x + x0,-y + y0});
				res.SetValue(obj, new int[]{ y + x0,-x + y0});
				y++;
			if(radErr<0) radErr+=2*y+1;
			else{ x--; radErr+= 2*(y-x)+1;}
			}
			
			if(floodFill)
			{
				if(floodFillObj==null){ floodFillObj = obj; }
				res.QueueFloodFill(new int[]{x0,y0}, emptyObj, floodFillObj);
			}
			
			return res;
		}
		/** END OF #########################################################################################**/
		
		
		
		
		
	}
}
