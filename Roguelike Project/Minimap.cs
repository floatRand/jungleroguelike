﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Minimap.
	/// </summary>
	public static class Minimap
	{

	
		
		
		public static Glyph[,] GenerateMiniMap( this Map map, int divider = 5)
		{
			Glyph[,] mapGlyphs;
			int w0 = map.mapdata.GetLength(0); int h0 = map.mapdata.GetLength(1);
			int w1 = w0/divider; int h1 = h0/divider;
			
			mapGlyphs = new Glyph[w1,h1];
			int traversableNum;
			for(int i=0; i<w1; i++){
			for(int j=0; j<h1; j++){
					traversableNum = 0;
					for(int x=0;x<divider;x++){
					for(int y=0;y<divider;y++){
							if(i*divider+x<w0 && j*divider+y<h0){
								if(map.mapdata[i*divider+x,j*divider+y].solidity==TileSolidity.none 
								   && map.mapdata[i*divider+x,j*divider+y].tileMemory.memorized) traversableNum++;
							}
					}
					}
					if(traversableNum > divider*4-divider/2) mapGlyphs[i,j] = new Glyph((char)219);
					else if(traversableNum > divider*3-divider/2) mapGlyphs[i,j] = new Glyph((char)178);
					else if(traversableNum > divider*2-divider/2) mapGlyphs[i,j] = new Glyph((char)177);	 	
					else if(traversableNum > divider*1-divider/2)	mapGlyphs[i,j] = new Glyph(1,(char)177, new Colour(155,155,155), new Colour(0,0,0));
					else mapGlyphs[i,j] = new Glyph(' ');
			}
			}
			mapGlyphs[ gameManager.Instance.playerX / divider, gameManager.Instance.playerY / divider ] = new Glyph(1,'@', new Colour(0,255,0), new Colour(0,0,0));
			return mapGlyphs;
		}


	}
}
