﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{

	public class Player : CreatureBase
	{
		
		public List<CreatureBase> CreaturesinFOV;
		public CreatureBase recentTarget;
		
		public Player() : base()
		{
			InitializeStats();
			glyph = new Glyph ( 1000, '@', new Colour(255,255,255),  
			                   new Colour(0,0,0,0), false, 1000  );
			ApplyTemplate(new Template_Human(), true);
			SetRace();
			name = "Player";
			IsPlayer = true;
			currentHP = maxHP; currentEP = maxEP;
			mainAttack = new List<Attack>()
			{
				new Attack( new Die(1,6), new Brand_Vanilla(), "fist", AttackDescriptor.Fist,  0.5f, 0.5f, 10 ),
				new Attack( new Die(1,6), new Brand_Vanilla(), "fist", AttackDescriptor.Fist,  0.5f, 0.5f, 10 )
			};
			
			equipment.slots = new List<EquipmentSlot>() // standard for now......
			{
				new EquipmentSlot(EquipSlot.Head,this),
				new EquipmentSlot(EquipSlot.Mask,this),
				new EquipmentSlot(EquipSlot.Back,this),
				new EquipmentSlot(EquipSlot.Body,this),
				new EquipmentSlot(EquipSlot.Hands,this),
				new EquipmentSlot(EquipSlot.Leg,this),
				new EquipmentSlot(EquipSlot.Finger,this),
				new EquipmentSlot(EquipSlot.Finger,this),
				new EquipmentSlot(EquipSlot.Neck,this),
				new EquipmentSlot(EquipSlot.Weapon,this),
				new EquipmentSlot(EquipSlot.Weapon,this)
			};
			
			
			for(int i=0;i<60;i++){
			inventory.AddItem(new ItemBase());
			}
			
			//equipment.AutoEquipItem(inventory.slots[0]);
			
			abilities.LearnAbility(new TestAbilityHeal());
			InflictDuration(new DUR_Poison(300,1,this,"whim of Xom"));
			InflictDuration(new DUR_Paralyzed(300,1,this,"whim of Xom"));
		}
		
		public bool HandleMovement(int dx, int dy)
		{
			CreatureBase getCreature;
			
			if(!gameManager.Instance.currentMap.IsCreatureOnTile(x+dx,y+dy)){
				if(IsSquarePassable(x+dx,y+dy,gameManager.Instance.currentMap)){ SetAction( new turnAction( delegate{ Move(dx, dy); },moveSpeed,0) ); return true; }
				else return false;
			}
			else{
				getCreature = gameManager.Instance.currentMap.GetCreatureOnTile(x+dx,y+dy);
				if(getCreature!=null){ SetAction( new turnAction( delegate{ Attack(getCreature); },attackSpeed,0) ); recentTarget = getCreature; return true; }
			}
		
		return false; // something went wrong, nothing happened
		}
		
		public void TestDropRecentTarget()
		{
			if(recentTarget!=null)
			{
				if(!recentTarget.alive || !recentTarget.inFOV){ recentTarget = null; } // drop target...
			}
			return;
			
		}
		
		
		
		
	}
}
