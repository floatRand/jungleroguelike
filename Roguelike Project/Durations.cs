﻿
using System;

namespace Roguelike_Project
{
	public enum DurationType{Poison, Disease, Magical, Curse, Natural, Other}
	public enum DurationNature{Beneficial, Neutral, Detrimental}
	
	public class DurationBase : IEquatable<DurationBase>
	{
		public string shortName, Name, description;
		public Colour colour;
		public int duration, severity; 
		public DurationNature nature;
		public DurationType durType;
		public bool permanent, expired, initialized; public CreatureBase target;
		
		public string source;
		public DurationBase(int dur, int sever, CreatureBase targt, string source, bool perm = false)
		{duration = dur; severity = sever; permanent=perm; expired=false; initialized = false; target = targt; colour = new Colour(255,255,255);}
		public virtual void OnInflict(){}
		public virtual void OnLengthen(int dur, int sev){} // subsequent hits
		public virtual void Effect(){} // the effect?
		public virtual void OnRemove(bool nonNaturalExperition){} // what happens on removal?
		
		public void ProcessDuration(){
			if(!initialized){ OnInflict(); initialized = true; }
			if(!expired && initialized){   
				Effect();
				if(!permanent) duration--;
				if(duration<0){ OnRemove(false); expired = true;  }
			}
		
		}
		public bool Equals(DurationBase other){return (other.Name==Name);}
		
		
	}
}
