﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Camera.
	/// </summary>
	public class Camera
	{
		public int x, y;
		
		public Camera()
		{
			x = 0; y = 0;
		}
	}
}
