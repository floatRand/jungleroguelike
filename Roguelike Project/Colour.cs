﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Colour.
	/// </summary>
	/// 
	public enum ColourName{White, GUIDefault, Red, Green, Blue, Black, Pink, Transparent, Random}
	
	public struct Colour
	{
		
		public byte red,green,blue,alpha;
		
		public Colour(byte R, byte G, byte B, byte A = 255 )
		{
			red = R; green = G; blue = B; alpha = A;
		}
		
		public Colour(bool yes, int R, int G, int B, int A = 255 )
		{
			red = (byte)R; green = (byte)G; blue = (byte)B; alpha = (byte)A;
		}
		
		public static Colour ByName( ColourName name )
		{
			switch(name){
				case ColourName.White: return new Colour(255,255,255);
				case ColourName.GUIDefault: return new Colour(155,175,155); // 155 175 155
				case ColourName.Red: return new Colour(255,0,0);
				case ColourName.Green: return new Colour(0,255,0);
				case ColourName.Blue: return new Colour(0,0,255);
				case ColourName.Black: return new Colour(0,0,0);
				case ColourName.Pink: return new Colour(255,166,166); // By request
				case ColourName.Random: return new Colour((byte)RNG.GetValue(0,255),(byte)RNG.GetValue(0,255),(byte)RNG.GetValue(0,255));
				case ColourName.Transparent: return new Colour(0,0,0,0);
				default: return new Colour(0,0,0);
			}
		}
		
		public static Colour Invert( Colour col )
		{ return new Colour( (byte)(255-col.red), (byte)(255-col.green),  (byte)(255-col.blue), col.alpha );	}
		
		public static Colour Multiply( Colour origcolour, Colour overlayCol )
		{   
			float newRed = GetCFloat(origcolour.red)*GetCFloat(overlayCol.red);
			float newGreen = GetCFloat(origcolour.green)*GetCFloat(overlayCol.green);
			float newBlue = GetCFloat(origcolour.blue)*GetCFloat(overlayCol.blue);
			return new Colour(GetCByte(newRed), GetCByte(newGreen), GetCByte(newBlue), origcolour.alpha );
		}
		
		public static Colour GrayScaleDarker( Colour origcolour )
		{
			float newColval = ( (float)origcolour.blue + (float)origcolour.red + (float)origcolour.green  ) / 6.0f;
			return new Colour(lT255(newColval),lT255(newColval),lT255(newColval),origcolour.alpha);
		}
		
		public static byte lT255(int k){ if(k<0) return 0; else if(k>255) return 255; else return (byte)k; } // limitTo0-255
		public static byte lT255(float k){ if(k<0) return 0; else if(k>255) return 255; else return (byte)k; } // limitTo0-255
		private static float GetCFloat(byte col){ return ((float)(col))/(255.0f); } // get colour float
		private static byte GetCByte(float col){ return lT255((int)(col*255)); }
		static public SFML.Graphics.Color ConvertToSFMLColour(Colour col){return new SFML.Graphics.Color( col.red, col.green, col.blue, col.alpha );}
		
	}
	
}
