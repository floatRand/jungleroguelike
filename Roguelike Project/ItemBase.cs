﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	public enum ItemType{Weapon, Armour, Trinket, Useable, Other}
	public enum ItemSubType{
							Evokable, // use
							Potion, // use
							Scroll, // use
							Book, // use
							Ring, // trink
							Amulet, // trink
							Armour, // armour
							MeleeWpn, // weapon
							RangedWpn, // weapon
							Shield, // weapon
							Ammo, // other
							Key, // other
							Other // what the hell
							}
	
	public enum WeaponCategory{ LBlade, SBlade, Mace, Axe, Polearm, Staff, Unarmed, Whip, // melee
								Bow, Crossbow, Gun, Launcher, Throwable, Blowgun, // ranged
								SShield, MShield, LShield, // shield
								none
								}
	public enum EquipSlot{ Weapon, Mask, Head, Hands, Body, Leg, Feet, Back, Finger, Neck, Harness, Other, none }
	
	public enum AmmoCategory{ Arrow, Bolt, Bullet, Needle, Thrown, none }
	
	[Serializable()]
	public class ItemBase
	{
		public string name, unid_name, description, baseName, IDENT;
		public bool equippable, equipped, onGround, cursed, autoPickUp, unique, stackable, unID;
		public Glyph glyph;
		
		public int worth, x, y; // determines things for item, also position
		public float mass; // in kilograms
		
		public int durability, maxDurability, enchantmentLevel;
		
		public List<ItemEgoBase> egos; // EGOS!
		
		//Weapon things
		public int LimbsReq; // how many hands you need to wear/use this properly, taken from main attacks. Arka could have things that take 4 hands!
		public int accuracyRating, minDelay, maxDelay; 
		public WeaponCategory weaponCategory;
		public AmmoCategory ammoCategory;
		public BrandBase brand;
		public Die meleeDmgDie, throwDmgDie; // defined for everything...
		////////////////////////
		//Armour things
		public int EVpenalty, DR; 
		public int encumberance; 
		public EquipSlot equipSlot, equippedOn;
		public int DonTime;
		////////////////////////
		//Throw
		public int throwDestX, throwDestY;
		public bool flying;
		/////////////////////////
		//Shield things
		public int blockRating;
		////////////////////////
		//Other
		public int charges, rechargeCD;
		public bool recharging;
		
		public bool inFov;
		
		public ItemBase()
		{
			IDENT = "hotpants_01";
			equippable = true; equipped = false; onGround = false; cursed = false; unique = false; autoPickUp = false; stackable = false; unID = true;
			glyph = new Glyph(0,'?',Colour.ByName(ColourName.Green), new Colour(0,0,0,0));
			worth = 0; x = 0; y = 0; mass = 0; DonTime = 10;
			maxDurability = 100; durability = 100; enchantmentLevel = 0;
			meleeDmgDie = new Die(1,2); throwDmgDie = new Die(1,2);
			LimbsReq = 1; accuracyRating = 0; minDelay = 10; maxDelay = 10; weaponCategory = WeaponCategory.none; ammoCategory=AmmoCategory.none;
			brand = new Brand_Vanilla(); EVpenalty = 0; DR = 1; encumberance = 0; equipSlot = EquipSlot.Head;
			throwDestX = 0; throwDestY = 0; flying = false; blockRating = 0; charges = 0; inFov = false;
			equippedOn = EquipSlot.Head; recharging = false; rechargeCD = 0;
			egos = new List<ItemEgoBase>();
			//if(RNG.Coinflip())egos.Add(new EGO_test(this));
			name = "Pants "+CreateEgoBlurp(); unid_name = "Strange Pants "+CreateEgoBlurp(); description = "Pants."; baseName = "pants";
		}
		
		public ItemBase(int x0, int y0) : this() // place on ground
		{
			x = x0; y = y0; onGround = true;
		}
		
		public virtual void Use_Effect(CreatureBase holder){}
		public virtual void Equip_Effect(CreatureBase holder){
			foreach(ItemEgoBase ego in egos){ ego.Equip_Effect(holder); }
		}
		public virtual void Worn_Effect(CreatureBase holder){
			foreach(ItemEgoBase ego in egos){ if(ego.worn_effect) ego.Worn_Effect(holder); }
		}
		public virtual void Carried_Effect(CreatureBase holder){
			foreach(ItemEgoBase ego in egos){ if(ego.carried_effect) ego.Carried_Effect(holder); }
		}
		public virtual void Unequip_Effect(CreatureBase holder){
			foreach(ItemEgoBase ego in egos){ ego.Unequip_Effect(holder); }
		}
		public virtual void Destruction_Effect(CreatureBase holder){}
		public virtual void Melee_Effect(CreatureBase holder, CreatureBase target){}
		public virtual void Collision_Effect(CreatureBase holder, CreatureBase target){}
		public virtual void Block_Effect(CreatureBase holder, CreatureBase target){}
		
		public string GetName()
		{
			if(unID) return unid_name;
			else return name;
		}
		
		public bool Equals(ItemBase other){
			if(other.IDENT == IDENT) return true; else return false;
		}
		
		public bool MatchItemCoordinates(int x0, int y0)
		{
			if(x==x0 && y==y0) return true;
			return false;
		}
		
		public string CreateEgoBlurp()
		{
			if(egos.Count==0) return "";
			string egonameList = "{";
			foreach(ItemEgoBase ego in egos){ egonameList+=ego.shortName; }
			egonameList+="}";
			return egonameList;
		}
		
	}
	
	
	public static class miscItemMethod
	{
	
		public static bool PickupItem(this List<ItemBase> itemList, ItemBase it, Inventory inv) // picked up from map...
		{
			if(itemList.Contains(it)){
				if(inv.HasSpace()){
					it.onGround = false; it.inFov = false; 
						inv.AddItem(it);
						itemList.Remove(it);
						MSGH.Print("Picked up "+it.GetName(), it.glyph.charColor);
						return true;
					}
				else{ MSGH.Print("Your inventory is full."); return false;}
			}
			Console.WriteLine("Itemlist does not contain the item "+it.name);
			return false;
		}
	
	
	}

	
	
}
