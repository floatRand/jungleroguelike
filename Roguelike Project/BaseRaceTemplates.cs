﻿
using System;
	
namespace Roguelike_Project
{
		// In general, template should have total of +7 of scores, unless they have other crippling penalties
		
	/**#################################################################################################**/
 	/**########################################## H U M A N ############################################**/
	/**#################################################################################################**/
	class Template_Human : CTemplate
	{
		public Template_Human()
		{	
			Init(); // required
			rName = "Human";  rNamePlural = "Humans";
			adjust_STR =  0;
			adjust_DEX =  0;
			adjust_FORT = 0;
			adjust_INT =  4;
			adjust_PER =  1;
			adjust_WILL = 4;
			MagicAffinityOverride = 10; // humans have highest magic affinity of all races
			isBaseRace = true;
		}
		override protected void AddFlags(CreatureBase creature)
		{
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.CannotCast);
					creature.AddcFlag(CreatureFlag.RejectedbyGods);
		}
		override protected void AddAbility(CreatureBase creature)
		{
		//Adrenaline Rush
		}
	}
	/**#################################################################################################**/
 	/**########################################## S L A U D ############################################**/
	/**#################################################################################################**/
	class Template_Slaud : CTemplate
	{
		public Template_Slaud()
		{
			Init();
			rName = "Slaud";  rNamePlural = "Slauds";
			adjust_STR =   0;
			adjust_DEX =  -1;
			adjust_FORT =  4;
			adjust_INT =   2;
			adjust_PER =   0;
			adjust_WILL =  0;
			MagicAffinityOverride = 5; // relatively good
			isBaseRace = true;
		}
		override protected void AddFlags(CreatureBase creature)
		{
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.sicknessImmunity);
					creature.AddcFlag(CreatureFlag.poisonImmunity);
		}
	}
	/**#################################################################################################**/
 	/**########################################## V A R P U ############################################**/
	/**#################################################################################################**/
	class Template_Varpu : CTemplate
	{
		public Template_Varpu()
		{
			Init();
			rName = "Varpu";  rNamePlural = "Varpus";
			adjust_STR =  -1;
			adjust_DEX =  +2;
			adjust_FORT = -2;
			adjust_INT =   2;
			adjust_PER =   2;
			adjust_WILL =  2;
			MagicAffinityOverride = 5; // relatively good
			isBaseRace = true;
		}
		override protected void AddFlags(CreatureBase creature)
		{
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.WingedFlight);
					creature.AddcFlag(CreatureFlag.Insufferable);
		}
		override protected void AddAbility(CreatureBase creature)
		{
		//Flight
		}
	}
	/**#################################################################################################**/
 	/**######################################## R U S T A D O ##########################################**/
	/**#################################################################################################**/
	class Template_Rustado : CTemplate
	{
		public Template_Rustado()
		{
			Init();
			rName = "Rustado";  rNamePlural = "Rustados";
			adjust_STR =  -2;
			adjust_DEX =   3;
			adjust_FORT = -2;
			adjust_INT =   0;
			adjust_PER =   4;
			adjust_WILL =  0;
			adjustSpeed =  1;
			MagicAffinityOverride = 3;
			isBaseRace = true;
			newSize = CreatureSize.Small;
		}
		override protected void AddFlags(CreatureBase creature){
					creature.AddcFlag(CreatureFlag.Speaks);
					
		}
		override protected void AddAbility(CreatureBase creature){
		// Bloodrage
		}
	}
	/**#################################################################################################**/
 	/**########################################## F A R D A ############################################**/
	/**#################################################################################################**/
	class Template_Farda : CTemplate
	{
		public Template_Farda()
		{
			Init();
			rName = "Farda";  rNamePlural = "Farda";
			adjust_STR =   1;
			adjust_DEX =   1;
			adjust_FORT =  0;
			adjust_INT =   1;
			adjust_PER =   1;
			adjust_WILL =  1;
			MagicAffinityOverride = 4; 
			isBaseRace = true;
			newType = CreatureType.Plant;
		}
		override protected void AddFlags(CreatureBase creature){
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.EschewsMetal);
					creature.AddcFlag(CreatureFlag.DarkVision);
					creature.AddcFlag(CreatureFlag.mistProof);
					creature.AddcFlag(CreatureFlag.UCBrand_Venom);
					
		}
	}
	/**#################################################################################################**/
 	/**###################################### D R A C O N I A N  #######################################**/
	/**#################################################################################################**/
	class Template_Draconian : CTemplate
	{
		public Template_Draconian()
		{
			Init();
			rName = "Draconian"; rNamePlural = "Draconians";
			adjust_STR =   2;
			adjust_DEX =   1;
			adjust_FORT =  2;
			adjust_INT =  -1;
			adjust_PER =   0;
			adjust_WILL =  0;
			MagicAffinityOverride = 6; 
			isBaseRace = true;
		}
		override protected void AddFlags(CreatureBase creature){
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.DarkVision);
					creature.AddcFlag(CreatureFlag.Draconic);
		}
		override protected void AddAbility(CreatureBase creature){
		// Breathweapon
		}
	}
	/**#################################################################################################**/
 	/**########################################### A R K A  ############################################**/
	/**#################################################################################################**/
	class Template_Antmen : CTemplate
	{
		public Template_Antmen()
		{
			Init();
			rName = "Arka";  rNamePlural = "Arkas";
			adjust_STR =   4;
			adjust_DEX =  -1;
			adjust_FORT =  3;
			adjust_INT =  -1;
			adjust_PER =   0;
			adjust_WILL = -1;
			MagicAffinityOverride = 3; 
			isBaseRace = true;
			newShape = CreatureShape.FourArmedHumanoid;
		}
		override protected void AddFlags(CreatureBase creature){
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.SeesInvisible);
		}
	}
	/**#################################################################################################**/
 	/**######################################## J A V D R A  ###########################################**/
	/**#################################################################################################**/
	class Template_Javdra : CTemplate
	{
		public Template_Javdra()
		{
			Init();
			rName = "Javdra";  rNamePlural = "Javdras";
			adjust_STR =   4;
			adjust_DEX =  -2;
			adjust_FORT =  4;
			adjust_INT =   2;
			adjust_PER =   2;
			adjust_WILL =  2;
			MagicAffinityOverride = 4; 
			isBaseRace = true;
			newShape = CreatureShape.Javdra;
			newSize = CreatureSize.Large;
		}
		override protected void AddFlags(CreatureBase creature){
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.DarkVision);
					creature.AddcFlag(CreatureFlag.sicknessImmunity);
		}
	}
	/**#################################################################################################**/
 	/**############################### W I N G E D   S E R P E N T  ####################################**/
	/**#################################################################################################**/
	class Template_WSerpent : CTemplate
	{
		public Template_WSerpent()
		{
			Init();
			rName = "Winged Serpent";  rNamePlural = "Winged Serpents";
			adjust_STR =  -2;
			adjust_DEX =   4;
			adjust_FORT = -1;
			adjust_INT =   3;
			adjust_PER =   1;
			adjust_WILL =  3;
			MagicAffinityOverride = 8; 
			isBaseRace = true;
			newShape = CreatureShape.Serpent;
			newSize = CreatureSize.Small;
		}
		override protected void AddFlags(CreatureBase creature){
					creature.AddcFlag(CreatureFlag.Speaks);
					creature.AddcFlag(CreatureFlag.DarkVision);
					creature.AddcFlag(CreatureFlag.Permaflight);
		}
	}
	/**#################################################################################################**/
	/**#################################################################################################**/
}
