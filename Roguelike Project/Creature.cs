﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{

	public enum CreatureType { Living, Unliving, Outsider, Plant, Construct, Magical, Abomination };
	public enum CreatureSize { Miniscule, Tiny, Small, Medium, Large, Huge, Gargantuan, Vast };
	public enum CreatureFlag { Speaks, EschewsEquipment, Unliving, Deaf, Blind, WingedFlight, 
							   Levitates, SeesInvisible, DarkVision, Intangible, Chaotic, IgnoresNeeds, Glowing, Noisy, mistProof,
							   CannotCast, RejectedbyGods, Metallic, NonIntelligent, SuperHeavy, sicknessImmunity, poisonImmunity,
							   Insufferable, EschewsMetal, Permaflight, Draconic, Digs, NoClipping, Spellcaster,
							   // Unarmed attack brands
							   UCBrand_Venom
	};
	public enum CreatureShape { Humanoid, Quadruped, Serpent, Orb, Shapeless, Shifting, FourArmedHumanoid, Javdra };
	
	public enum CreatureIntelligence { Galosh, Low, Normal, High, Cunning, Special }; // type of AI its using
	public enum CreatureFaction{ Player, Forest, Sheol, None, KillEveryone };
	public enum AbilityScore{strength, dexterity, intelligence, will, perception, fortitude}
	public enum CreatureGender{ male, female, plural, other } // people will hang me for this
	[Serializable()]
	public class CreatureBase
	{
		public Glyph glyph;
		
		public CreatureType creatureType;
		public CTemplate MainTemplate;
		public CTemplate tempTemplate; // transformations etc.
		public List<CTemplate> Templates;
		public List<CreatureFlag> cFlags;
		
		public CreatureIntelligence AItype;
		public CreatureFaction Faction;
		public CreatureSize size;
		public CreatureShape shape;
		public CreatureGender gender; // Before someone asks, this is for descriptors and some interesting item-interaction 
									  // like intelligent weapon that refuses to strike women
		
		public int x, y;
		
		public bool alive;
		
		public int strength, dexterity, intelligence, will, perception, fortitude;
		public int final_strength, final_dexterity, final_intelligence, final_will, final_perception, final_fortitude, final_moveSpeed;
		public int DR, EV;
		
		public int HD, hpdie, Experience, XPmod;
		public int XP_on_kill; 
		public int ID;
		public int currentHP, currentEP, maxHP, maxEP;
		public int magicAffinity;
		public sbyte Alignment; // 0 = neutral, <0 evil, etc. Some spells depend on this.
		
		public int moveSpeed, attackSpeed; // 10 = 1 turn per square?
		public bool IsPlayer, inFOV, NonExistant; 
		
		public int stealthScore;
		public int luck;
		
		public string name;
		public string description;
		public string raceName, raceNamePlural;
		
		public bool refreshCreatureStats;
		
		public turnAction currentAction;
		
		public List<Attack> mainAttack; public int attackLimit = 2; 
		public List<Attack> auxAttack;
		public List<Ability> abilities;
		
		public Inventory inventory;
		public Equipment equipment;
		public List<DurationBase> durations;
		
		public CreatureBase()
		{
			glyph = new Glyph('?');
			name = "Glitch"; raceName = "Bug"; raceNamePlural = "Bugs";
			cFlags = new List<CreatureFlag>();
			InitializeStats();
			refreshCreatureStats = false;
			currentAction = new turnAction( delegate{},0,0 );
			currentAction.state = ActionState.Finished;
			IsPlayer = false; inFOV = false;
			mainAttack = new List<Attack>(); auxAttack = new List<Attack>();
			inventory = new Inventory(60);
			abilities = new List<Ability>();
			equipment = new Equipment(this); alive = true;
			NonExistant = false;
			durations = new List<DurationBase>();
			x = 0; y = 0; XPmod = 1; Experience = 0; Alignment = 0;
			HD = 1; XP_on_kill = 0; ID = -1; currentEP = 0; currentHP = 1;
			creatureType = CreatureType.Living; AItype = CreatureIntelligence.Galosh;
			size = CreatureSize.Medium; Faction = CreatureFaction.None;
			shape = CreatureShape.Shapeless; 
			final_dexterity = 0; final_fortitude = 0; final_strength = 0;
			final_perception = 0; final_strength = 0; final_will = 0; final_moveSpeed = 0;
			luck = 0;
			gender = CreatureGender.other;
		}
		
		
		public void AddcFlag(CreatureFlag flag){
			if(cFlags.Contains(flag)) cFlags.Add(flag);
		}
		public bool HascFlag(CreatureFlag flag){
			if(cFlags.Contains(flag)) return true;
			return false;
		}
		
		public void RemovecFlag(CreatureFlag flag){
			if(cFlags.Contains(flag)) cFlags.Remove(flag);
		}
		
		public bool ApplyTemplate(CTemplate templ, bool baserace=false){
			if(baserace){MainTemplate = templ; return true;}
			if(!Templates.Contains(templ)){Templates.Add(templ); return true;}
			return false;
		}
		
		public void InitializeStats() // initializes everything
		{
			strength = 10; dexterity = 10; fortitude = 10; will = 10; intelligence = 10; perception = 10;
			DR = 0; EV = 10; maxHP = HD * hpdie; maxEP = HD * magicAffinity; magicAffinity = 0;
			moveSpeed = 10; stealthScore = 0; 
			attackSpeed = 10; luck = 0;
		}
		
		public void SetRace()
		{
			raceName = MainTemplate.rName; raceNamePlural = MainTemplate.rNamePlural;
			magicAffinity = MainTemplate.MagicAffinityOverride;
			hpdie = MainTemplate.HPdie;
			maxHP = HD * hpdie; maxEP = HD * magicAffinity;
			moveSpeed -= MainTemplate.adjustSpeed;
			
			strength += MainTemplate.adjust_STR;
			dexterity += MainTemplate.adjust_DEX;
			fortitude += MainTemplate.adjust_FORT;
			intelligence += MainTemplate.adjust_INT;
			will += MainTemplate.adjust_WILL;
			perception += MainTemplate.adjust_FORT;
			
		}
		
		protected void spawnFinalize(int x0, int y0){
			x = x0; y = y0; 
			currentHP = maxHP; currentEP = maxEP;
		}
		
		public void AlterAbilityScore(AbilityScore theScore, int value)
		{
			switch(theScore){
					case AbilityScore.strength: final_strength=final_strength+value; return;
					case AbilityScore.dexterity: final_dexterity=final_dexterity+value; return;
					case AbilityScore.fortitude: final_fortitude=final_fortitude+value; return;
					case AbilityScore.intelligence: final_intelligence=final_intelligence+value; return;
					case AbilityScore.perception: final_perception=final_perception+value; return;
					case AbilityScore.will: final_will=final_will+value; return;
					default: return;
			}
		}
		public int GetAbilityScore(AbilityScore theScore, bool basescore = false)
		{
			if(!basescore){
			switch(theScore){
					case AbilityScore.strength: return final_strength;
					case AbilityScore.dexterity: return final_dexterity;
					case AbilityScore.fortitude: return final_fortitude;
					case AbilityScore.intelligence: return final_intelligence;
					case AbilityScore.perception: return final_perception;
					case AbilityScore.will: return final_will;
					default: return 0;
				}}
			else{
				switch(theScore){
					case AbilityScore.strength: return strength;
					case AbilityScore.dexterity: return dexterity;
					case AbilityScore.fortitude: return fortitude;
					case AbilityScore.intelligence: return intelligence;
					case AbilityScore.perception: return perception;
					case AbilityScore.will: return will;
					default: return 0;
				}
			}
		}
		
		public bool IsFreeToAct()
		{
			if(currentAction.state == ActionState.Finished) return true;
			return false;
		}
		
		public bool IsSquarePassable(int x1, int y1, Map map)
		{
			if(cFlags.Contains(CreatureFlag.NoClipping)) return true;
			if(x1>=0 && x1<map.mapdata.GetLength(0)){
				if(y1>=0 && y1<map.mapdata.GetLength(1)){
					// Check if it is solid
			if(map.mapdata[x1,y1].solidity==TileSolidity.solid && !cFlags.Contains(CreatureFlag.Digs)) return false;
			if(map.mapdata[x1,y1].solidity==TileSolidity.indestructable) return false;
			if(map.mapdata[x1,y1].solidity==TileSolidity.brittle) return false;		
			if(map.IsCreatureOnTile(x1,y1)) return false;
			   	}}
			return true;
		}
		
		public bool IsSquareDangerous(int x1, int y1, Tile[,] mapInfo)
		{
			if(cFlags.Contains(CreatureFlag.NoClipping)) return true;
			if(x1>=0 && x1<mapInfo.GetLength(0)){
				if(y1>=0 && y1<mapInfo.GetLength(1)){
					// Check if it is dagnerous
					// LATER ON
			   	}}
			return true;
		}
		
		public void Move(int x1, int y1){
			if(IsSquarePassable(x+x1,y+y1,gameManager.Instance.currentMap)){x+=x1; y+=y1;}
		}
		
		public void SetAction( turnAction act, bool forceAction = false )
		{
			if(forceAction || IsFreeToAct()){
			currentAction = act;
			gameManager.Instance.ActionQueue.Add( act );
			if(IsPlayer){GTime.Get().PlayerActed();}
			}
			return;
		}
		
		public virtual bool Draw(GlyphRenderer renderer)
		{
		return renderer.InsertGlyph( x, y, glyph, true );
		}
		
		public bool MatchCreatureCoordinates(int x0, int y0)
		{
			if(x==x0 && y==y0) return true;
			return false;
		}
		
		public bool InSquareArea(int x0, int y0, int radi)
		{
			if(x>=x0-radi && x<=x0+radi && y>=y0-radi && y<=y0+radi ) return true;
			return false;
		}
		
		public void Update()
		{
			CalculateStats();
			equipment.ProcessEquipEffects(this);
			
			foreach(DurationBase elm in durations){ elm.ProcessDuration(); } // process the durations
			PurgeExpiredDurations();
			if(IsFreeToAct() && !IsPlayer) AIUpdate();

			refreshCreatureStats = false;
		}
		
		protected void CalculateStats()
		{
			final_strength = strength; 
			final_dexterity = dexterity;
			final_perception = perception; 
			final_will = will; 
			final_fortitude = fortitude; 
			final_intelligence = intelligence;
			final_moveSpeed = moveSpeed;
			stealthScore = 0;
			luck = 0;
			DR = 0; EV = 10;
			if(IsPlayer){ maxHP = maxHP = HD * hpdie; maxEP = HD * magicAffinity; } // don't do this for foes for now...
		}
		
		public static List<CreatureBase> CurrentCreatureList()
		{return gameManager.Instance.currentMap.creatures;}
		
		
		public void AIUpdate()
		{
		
			int randAction = RNG.GetValue(1,5);
			//gameManager.Instance.currentMap.shadowMap.AddLight(x,y,3);
			// just random movement for now
			if(randAction==1) SetAction( new turnAction( 
			               delegate{ Move(RNG.GetValue(-1,1), RNG.GetValue(-1,1)); if(inFOV) MSGH.Print(name+" rolls around without an AI."); }, moveSpeed,0,"rolling") );
			if(randAction==2) SetAction( new turnAction( 
			               delegate{ Move(RNG.GetValue(-1,1), RNG.GetValue(-1,1)); if(inFOV) MSGH.Print(name+" vibrates buggily through space."); },   moveSpeed,0,"vibrating") );
			if(randAction==3) SetAction( new turnAction( 
			               delegate{ Move(RNG.GetValue(-1,1), RNG.GetValue(-1,1)); if(inFOV) MSGH.Print(name+" twiddles "+GetPron(GPron.g)+" thumbs."); },  moveSpeed,0,"getting displaced") );
			if(randAction==4) SetAction( new turnAction( 
			               delegate{ Move(RNG.GetValue(-1,1), RNG.GetValue(-1,1)); if(inFOV) MSGH.Print(name+" tries to hug you."); }, moveSpeed/2,moveSpeed/2,"trying to hug") );
			if(randAction==5) SetAction( new turnAction( 
			               delegate{ if(inFOV) MSGH.Print(name+" is doing absolutely nothing"); }, 2, 0,"doing nothing") );
		}
		
	
		public void Attack(CreatureBase defender) // regular attack
		{
			int attackRoll;
			int attackCoEff = 0;
			//REGULAR ATTACK
			if(mainAttack.Count<1 && IsPlayer){ MSGH.Print("You just angrily glare at "+defender.name); }
			foreach(Attack atk in mainAttack)
			{
				attackRoll = RNG.RollDie(1,20) + HD + atk.accuracy;
				if(attackRoll>=defender.EV)
				{
					defender.Defend( this, atk );
					MSGH.Print(Desciptors.DescribeAttack(atk.attackDescriptor, atk.name, this, defender ));
				}
				else{
					if(IsPlayer) MSGH.Print("You miss");
					else MSGH.Print(name+" misses");
				}
			}
			// AUX ATTACK
			foreach(Attack atk in auxAttack)
			{
				attackRoll = RNG.RollDie(1,20) + HD + atk.accuracy;
				if(attackRoll>=defender.EV)
				{
					attackCoEff-=4;
					defender.Defend( this, atk );
					MSGH.Print(Desciptors.DescribeAttack(atk.attackDescriptor, atk.name, this, defender ));
				}
				else{
					if(IsPlayer) MSGH.Print("You miss");
					else MSGH.Print(name+" misses");
				}
			}
			

		}
		
		public void Defend(CreatureBase attacker, Attack attack)
		{
			int damageTaken = attack.damageDie.Roll() + attack.damageBonus;
			damageTaken-=DR; 
			if(damageTaken<0){ damageTaken = 0; }
			
			currentHP = currentHP - damageTaken;
		}
		
		public int CalculateXP()
		{
			int tHD = HD; // we use HD to calculate the XP
			if(creatureType==CreatureType.Outsider || creatureType==CreatureType.Unliving) tHD++; //other holiness are troublesome
			if(mainAttack.Count > 2) tHD+=(int)Math.Floor((float)(mainAttack.Count)/2);   // if there are more than two attacks, increase virtual HD for calculation
			if(moveSpeed < 6) tHD++;
			if(cFlags.Contains(CreatureFlag.Spellcaster)){ tHD+=2; }
		
			return (10+(int)Math.Pow(tHD,3))*XPmod;
		}
		
		public void UseAbilityOnSelf(Ability ability)
		{
			bool success;
			success = ability.UseAbility(this,this);
			if(!success) ability.GenerateFailureMessage(this,this);
		}
		
		public void InflictDuration(DurationBase dur)
		{
			if(!durations.Contains(dur)){ durations.Add(dur); }
			else{ durations.Find(elm => elm.Equals(dur)).OnLengthen(dur.duration, dur.severity);  }
		}
		public void PurgeExpiredDurations(){durations.RemoveAll(elm => elm.expired);}
		
		public string GetPron( GPron pron )
		{
			string ret = "";
			switch(pron){
					case GPron.g:{ Gwords.genetives.TryGetValue(gender, out ret); break; }
					case GPron.o:{ Gwords.objectPronouns.TryGetValue(gender, out ret); break;  }
					case GPron.s:{ Gwords.subjectPronouns.TryGetValue(gender, out ret); break; }
			}
			return ret;
		}
		protected CreatureGender RandomMaleorFemale()
		{
			if(RNG.Coinflip()) return CreatureGender.male; return CreatureGender.female;
		}
		
		public void PlayerDamageColours()
		{
			if(currentHP>=maxHP){ glyph.charColor = new Colour(255,255,255); }
			else glyph.charColor = Desciptors.GetWoundColour( currentHP, maxHP );
		}
		
		
		
	}
}
