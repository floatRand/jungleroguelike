﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of MapGenerator.
	/// </summary>
	public static class MapGenerator
	{

	
		public static Tile[,] GenerateCave(uint x, uint y, int iterNumber = 8, int enclosingthreshold = 4){
		 Random rand = new Random();
			Tile[,] cave = new Tile[x,y];
			RandomFill(cave, new Tile(new Glyph( 1, '#', new Colour(255,255,255), new Colour(0,0,0)  ),1), 
			                 new Tile(new Glyph( 0, '.', new Colour(255,255,255), new Colour(10,20,10)  ),0, TileSolidity.none) );
			
			int iter = iterNumber; // number of iterations
			CreateBorders( cave, new Tile(new Glyph( 1, '#', new Colour(255,255,255), new Colour(0,0,0)  ),1)  );
			CreateBorders( cave, new Tile(new Glyph( 1, '#', new Colour(255,255,255), new Colour(0,0,0)  ),1),1, 1  );
		
			for(int a = 0; a<iter; a++ ){
			for (int i = 0; i < cave.GetLength(0)-1; i++){
  					  for (int j = 0; j <  cave.GetLength(1)-1; j++){
					  
						if((CountTilesAround(cave,(uint)i,(uint)j,1)>=4 || 
						    ( AnyTilesinRange(cave,(uint)i,(uint)j,2,1) && a<enclosingthreshold )) && cave[i,j].ID == 1){
							cave[i,j] = new Tile(new Glyph( 1, '#', new Colour(105,145,105), new Colour(10,20,10)  ),1); }
						else if(CountTilesAround(cave,(uint)i,(uint)j,1)>=5 && cave[i,j].ID != 1){ 
							cave[i,j] = new Tile(new Glyph( 1, '#', new Colour(105,145,105), new Colour(10,20,10)  ),1); }
						else{  cave[i,j] = new Tile(new Glyph( 0, '.', new Colour(10,25,25), new Colour(25,50,25)  ),0, TileSolidity.none);  }
					
					}
			}
			}
			CreateBorders( cave, new Tile(new Glyph( 3, (char)178, new Colour(155,155,155), new Colour(20,20,20)),3,TileSolidity.indestructable)  );
			
			
			return cave;
		}
		
		private static int CountTilesAround(Tile[,] map, uint x, uint y, ushort IDcheckedFor){
			int num = 0;
			uint maxY = (uint)map.GetLength(1);
			uint maxX = (uint)map.GetLength(0);
			if(x>0 && y>0){      if(map[x-1,y-1].ID == IDcheckedFor){ num++; }}  // upper left
			if(y>0){             if(map[x,y-1].ID   == IDcheckedFor){ num++; }} // up
			if(x>0){             if(map[x-1,y].ID   == IDcheckedFor){ num++; }} // left
			if(x<maxX && y>0){   if(map[x+1,y-1].ID == IDcheckedFor){ num++; }} // upper right
			if(x<maxX){ 		 if(map[x+1,y].ID   == IDcheckedFor){ num++; }} // right
			if(x<maxX && y<maxY){if(map[x+1,y+1].ID == IDcheckedFor){ num++; }} // lower right
			if(y<maxY){			 if(map[x,y+1].ID   == IDcheckedFor){ num++; }} // down
			if(x>0 && y<maxY){   if(map[x-1,y+1].ID == IDcheckedFor){ num++; }} // lower left
		    return num;
		}
		
		private static bool AnyTilesinRange(Tile[,] map, uint x, uint y, int range, ushort IDcheckedFor){
			
				for (int i = (int)x - range; i < (int)x + range; i++){
  					  for (int j = (int)y - range; j < (int)y + range; j++){
					
							if(i>=0 && j>=0 && i<map.GetLength(0) && j<map.GetLength(1)){
							if(map[i,j].ID == IDcheckedFor){ return true; }
							}
   						 }
				}
				return false;
		}
		
		
		
		private static void CreateBorders( Tile[,] map, Tile borderTile, int offsetx = 0, int offsety = 0){ 

			/// Offset is units from the borders of map.

			for (int i = offsetx; i < map.GetLength(0)-offsetx; i++){ map[i,offsety] = borderTile;}
			for (int i = offsety; i < map.GetLength(1)-offsety; i++){ map[offsetx,i] = borderTile;}
			for (int i = offsetx; i < map.GetLength(0)-offsetx; i++){ map[i,map.GetLength(1)-1-offsety] = borderTile;}
			for (int i = offsety; i < map.GetLength(1)-offsety; i++){ map[map.GetLength(0)-1-offsetx,i] = borderTile;}
			
				
			
		}
	
		private static void WallFill( Tile[,] map, Tile wallTile ){
			for (int i = 0; i < map.GetLength(0); i++){
  					  for (int j = 0; j <  map.GetLength(1); j++){
								map[i,j] = wallTile;
   						 }
			}
		}
		
		private static void RandomFill( Tile[,] map, Tile wallTile, Tile floorTile ){
			
			Random r = new Random();

			for (int i = 0; i < map.GetLength(0); i++){
  					  for (int j = 0; j <  map.GetLength(1); j++){
							if(r.Next(0,100)>65){	  map[i,j] = wallTile;		} // 65 is best!
							else{  map[i,j] = floorTile;   }
   						 }
			}
		}
		
		
		
		
		
		
		
		
	
	}
	
	
}
