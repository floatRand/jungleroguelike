﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// A creature templates! Basically, these are added on the creature, although some might stop you getting some.
	/// They alter statistics, HP die ( which decides hp ), etc. Basically, all races are worked as creature templates ( base template )
	/// Priorities kinda give on how "strong" the template is. Like undead templates are really strong since they override lot of normalities of characters, like making them "UNDEAD" creature rather than living
	/// </summary>
	public class CTemplate : IEquatable<CTemplate>
	{
		public int Priority;
		public int HPdie;
		
		public string rName, rNamePlural;
		
		public int adjust_STR, adjust_DEX, adjust_INT, adjust_FORT, adjust_WILL, adjust_PER, MagicAffinityOverride, adjustSpeed;
		public bool isBaseRace;
		public CreatureType newType;
		public CreatureSize newSize;
		public CreatureShape newShape;
		
		public bool OverrideShape, OverrideSize, OverrideType, OverrideDie;
		
		protected virtual void AddFlags(CreatureBase creature){}
		protected virtual void AddAbility(CreatureBase creature){}
		protected virtual void AddModifier(CreatureBase creature){}
		protected virtual void neutralizeFlags(CreatureBase creature){}
		
		protected void Init()
		{
			rName = "Empty"; HPdie = 6; Priority = 0;
			adjust_STR = 0; adjust_DEX = 0; adjust_INT = 0; adjust_FORT = 0; adjust_WILL = 0; adjust_PER = 0; MagicAffinityOverride = 0; adjustSpeed = 0;
			newType = CreatureType.Living; newSize = CreatureSize.Medium; isBaseRace = false; newShape = CreatureShape.Humanoid;
			OverrideDie = false; OverrideSize = false; OverrideType = false; OverrideShape = false;
		}
		
		public bool Equals(CTemplate other)
		{
			if(other.rName == rName) return true;
			else return false;
		}
		
	}
}
