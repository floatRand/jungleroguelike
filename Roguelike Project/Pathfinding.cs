﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{

	public enum PathfinderExclusionFlag{ None, Monster, Walls, Everything } 
	public enum PathfinderNodeState{ Untested, Open, Closed  }
	 
	public class PFNode{
		public int x, y;
		public bool passable;
		public float totalCost, manhattan, cost; // ( 1 )
		public float estimate { get{ return cost + manhattan;  }  }
		public PathfinderNodeState state;
		public PFNode parent;
		
		public PFNode( int x0, int y0, PFNode pare ){ x = x0; y = y0; parent = pare; state = PathfinderNodeState.Untested; passable = true; cost = 0; manhattan = 0; totalCost = 0;}
		public bool MatchCoord(int x0, int y0){ if(x0==x && y0==y) return true; return false; }
	}
	
	public class SearchParam
	{
		public int[] start;
		public int[] end;
		public Map map;
		public List<PFNode> openNodes, closedNodes;
		public SearchParam( int x0, int y0, int x, int y, Map mp )
		{
			start = new int[]{x0,y0};
			end = new int[]{x,y};
			map = mp;
			openNodes = new List<PFNode>(); closedNodes = new List<PFNode>();
		}
		
	}
	
	public static class Pathfinding
	{
		public static Map GetMap(){ return gameManager.Instance.currentMap; }
		
		public static List<int[]> moveDirs = new List<int[]>()
		{   
			new int[]{-1, -1},
			new int[]{-1, 0},
	     	new int[]{-1, 1},
			new int[]{0, -1},
			new int[]{0, 1},
			new int[]{1, -1},
			new int[]{1, 0},
			new int[]{1, 1}
		};
		
		private static List<int[]> getNearLocations(PFNode orignode, Map map)
		{
			List<int[]> nearPos = new List<int[]>();
			foreach(int[] dir in moveDirs){
				if(ValidTile(orignode.x+dir[0], orignode.y+dir[1],map)) nearPos.Add(new int[]{orignode.x+dir[0], orignode.y+dir[1]});
			} 
			return nearPos;
		}
		
		private static List<PFNode> GetNearValidNodes(PFNode orignode, SearchParam schParam)
		{
			List<PFNode> validNotes = new List<PFNode>();
			List<int[]> nearPos = new List<int[]>();
			nearPos = getNearLocations(orignode,schParam.map);
			
			foreach(int[] pos in nearPos)
			{
				PFNode testNode;
				testNode = new PFNode(pos[0],pos[1],orignode);  // create a new n
				if(schParam.openNodes.Exists(elm => elm.MatchCoord(pos[0],pos[1]))) testNode = schParam.openNodes.Find(elm => elm.MatchCoord(pos[0],pos[1]));
				if(schParam.closedNodes.Exists(elm => elm.MatchCoord(pos[0],pos[1]))) testNode = schParam.closedNodes.Find(elm => elm.MatchCoord(pos[0],pos[1]));
				
					if(testNode.state == PathfinderNodeState.Closed ) continue;
					if(testNode.state == PathfinderNodeState.Open ) continue;
					if(testNode.state == PathfinderNodeState.Untested)
					{
						testNode.parent = orignode;
						testNode.state = PathfinderNodeState.Open;
						testNode.manhattan = CalculateManhattanDist(testNode, schParam);
						testNode.cost = orignode.cost + 1;
						testNode.totalCost = testNode.cost+testNode.manhattan;
						
						schParam.openNodes.Add(testNode);
						validNotes.Add(testNode);
					}

			}  //foreach pos end
			
			return validNotes;
		}
		
		private static int CalculateManhattanDist(PFNode thenode, SearchParam sparam)
		{ return Math.Abs((int)(sparam.end[0]-thenode.x)) + Math.Abs((int)(sparam.end[1]-thenode.y)); }
		
		private static PFNode LowestNodeCost(SearchParam sparam)
		{
			float lowValue = float.MaxValue;
			PFNode selectNode = null;
			foreach(PFNode pt in sparam.openNodes)
			{ if(pt.totalCost<lowValue){lowValue = pt.totalCost; selectNode = pt;} }
			return selectNode;
		}
		private static PFNode LowestNodeCost(List<PFNode> nodes)
		{
			float lowValue = float.MaxValue;
			PFNode selectNode = null;
			foreach(PFNode pt in nodes)
			{ if(pt.totalCost<lowValue){lowValue = pt.totalCost; selectNode = pt;} }
			return selectNode;
		}
		
		
		public static bool ValidTile( int x1, int y1, Map map )
		{
			if(x1>=0 && x1<map.mapdata.GetLength(0) && y1>=0 && y1<map.mapdata.GetLength(1)){
			if(map.mapdata[x1,y1].solidity==TileSolidity.solid) return false;
			if(map.mapdata[x1,y1].solidity==TileSolidity.indestructable) return false;
			if(map.mapdata[x1,y1].solidity==TileSolidity.brittle) return false;		
			if(map.IsCreatureOnTile(x1,y1)) return false;
			}
			else return false; // outofbounds-situation, let's not.
			return true;
		}
		
		public static List<PFNode> FindPath(SearchParam sparam)
		{
			
			sparam.openNodes.Clear(); sparam.closedNodes.Clear();
			List<int[]> path = new List<int[]>();
			PFNode initialNode = new PFNode(sparam.start[0],sparam.start[1],null);
			initialNode.parent = initialNode; // no null pls
			sparam.openNodes.Add(initialNode);
			List<PFNode> openPath = new List<PFNode>();
			int giveUp = 1000;
			while(sparam.openNodes.Count!=0 && giveUp>0){
				PFNode curNode = LowestNodeCost(sparam);
				if(curNode.x == sparam.end[0] && curNode.y == sparam.end[1]){break;} // end reached
				curNode.state = PathfinderNodeState.Closed;
				sparam.openNodes.Remove(curNode); sparam.closedNodes.Add(curNode);
				GetNearValidNodes(curNode,sparam);
				giveUp--;
			}
		
			return sparam.closedNodes;
    	} /** END **/
		
		public static bool IsNodeAdjancent(int x, int y, PFNode node)
		{
			if(Math.Abs(node.x - x)<=1 && Math.Abs(node.y - y)<=1) return true;
			else return false;
		}
		
	
	}
}
