﻿
using System;


namespace Roguelike_Project
{
	/// <summary>
	/// A graphical representation of tile.
	/// Mind that ID is for purposes of graphical tiles.
    /// Renderer will take it and match it with corresponding graphic.
    /// Priority will decide will stuff be drawn over it.
	/// </summary>
	public struct Glyph : IEquatable<Glyph>
	{
		public char character;
		public ushort ID;
		public Colour charColor;
		public Colour bgColor;
		public bool glowing; 
		public ushort priority;
		public GTile graphicTile;
			
		public Glyph(ushort identif, char chara, Colour character_color, Colour background_color, bool glow = false, ushort prio = 100)
		{
			character = chara; charColor = character_color; bgColor = background_color;
			glowing = glow; 
			ID = identif;
			priority = prio;
			graphicTile = GTile.GTILE_NO_TILE;
		}
		public Glyph(char chara)
		{
			character = chara; charColor = new Colour(255,255,255); bgColor = new Colour(0,0,0);
			glowing = false; 
			ID = 0;
			priority = 0;
			graphicTile = GTile.GTILE_NO_TILE;
		}
		
		public bool Equals(Glyph other){
			Console.WriteLine("IT WAS CALLED, GLYPH ONE IS "+this.character+" AND OTHER IS "+other.character);
			if(this.character == other.character){ return true;  }
			return false;
		}
		
		
	}
}
