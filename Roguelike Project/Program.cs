﻿/*
 * Created by SharpDevelop.
 * User: Saska
 * Date: 4.4.2015
 * Time: 1:51
 */
using System;

namespace Roguelike_Project
{

	class Program
	{
			
		public static Game game;
		
		
		public static void Main(string[] args)
		{
			
			game = new Game();
			game.Run();
			
			
		}
		
	}
}