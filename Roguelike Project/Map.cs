﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Map.
	/// </summary>
	/// 
	public enum TileSolidity{ indestructable, solid, brittle, fluff, none}
	[Serializable()]
	public class Map
	{
		
		public Tile[,] mapdata;
		public uint sizeX, sizeY;
		public Tile w;
		public Tile f;
		public Glyph[,] glyphMap;
		public ShadowMap shadowMap;
				public List<CreatureBase> creatures;
				public List<ItemBase> items;
				public List<GasCloud> clouds;
				
		public Map(uint width, uint height)
		{
			sizeX = width; sizeY = height;
			w = new Tile(new Glyph( 0, '☼', new Colour(255,255,255), new Colour(0,0,0)  ),1);
			f = new Tile(new Glyph( 1, '.', new Colour(255,255,255), new Colour(0,0,0)  ),0);
			mapdata = MapGenerator.GenerateCave(width,height);
			ExtractGlyphMap();
			shadowMap = new ShadowMap(width,height,1);
			creatures = new List<CreatureBase>(); items = new List<ItemBase>(); clouds = new List<GasCloud>();
		}
		
		public void ExtractGlyphMap(){
			glyphMap = new Glyph[mapdata.GetLength(0),mapdata.GetLength(1)];
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					glyphMap[i,j] = mapdata[i,j].glyph;
				}
			}
		}
		
		public void SetMemorizedFeatures(Glyph[,] glmap, int x0, int y0)
		{
			for(int i = 0; i<glmap.GetLength(0); i++ ){
				for (int j = 0; j<glmap.GetLength(1); j++){
					if(i+x0>=0 && i+x0<mapdata.GetLength(0) && j+y0>=0 && j+y0<mapdata.GetLength(1) )
					{
						if(mapdata[i+x0,j+y0].tileMemory.memorized // memorized
						   && !mapdata[i+x0, j+y0].seen // and not directly seen
						  ) { glmap[i,j] = mapdata[i+x0,j+y0].tileMemory.glyph; }
					}
				}
			}
		}
		
		public void MagicMapping()
		{
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					mapdata[i,j].tileMemory.SetMemory(mapdata[i,j].glyph);
				}
			}
		}
		

		public bool IsCreatureOnTile( int x0, int y0 ){ return creatures.Exists( element => element.MatchCreatureCoordinates(x0,y0) ); }
		public CreatureBase GetCreatureOnTile( int x0, int y0 ) { return creatures.Find( element => element.MatchCreatureCoordinates(x0,y0) ); }
		
		public bool IsItemOnTile( int x0, int y0 ){ return items.Exists( element => element.MatchItemCoordinates(x0,y0) ); }
		public ItemBase GetItemOnTile(int x0, int y0 ){ return items.Find(element => element.MatchItemCoordinates(x0,y0)); }
		public List<ItemBase> GetItemsOnTile(int x0, int y0 ){ return items.FindAll(element => element.MatchItemCoordinates(x0,y0)); }
		
		public void CleanItemFovs(){ foreach( ItemBase item in items ) item.inFov = false; }
		
		public void UpdateClouds()
		{
			if( clouds.Count!=0 ) ClearGasStates();
			foreach( GasCloud cloud in clouds )
			{
				cloud.UpdateGas();
			}
			clouds.RemoveAll(elm => elm.cloudDissipated);
			
		}	
		
		private void ClearGasStates()
		{
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					mapdata[i,j].containsGas = false;
				}
			}
		}
		
		
		public void DrawClouds(GlyphRenderer rend)
		{
			foreach(GasCloud cloud in clouds)
			{
				cloud.DrawGas(rend);
			}
		}
		
		public void DrawItems(GlyphRenderer rend)
		{
			foreach(ItemBase item in items)
			{
				if(item.onGround) rend.InsertGlyph(item.x,item.y,item.glyph,true);
			}
		}
		
		
		
	}
	
	[Serializable()]
	public class Tile
	{
		public bool breakable;
		public ushort ID;
		public bool seen, containsGas, transparent;
		public TileSolidity solidity;
		public Glyph glyph;
		public TileMemory tileMemory;
		
		public Tile(Glyph gly, ushort ide, TileSolidity sol = TileSolidity.solid)
		{	
			glyph = gly;
			ID = ide;
			solidity = sol;
			tileMemory = new TileMemory();
			tileMemory.glyph = gly;
			containsGas = false;
			if(sol == TileSolidity.none || sol == TileSolidity.fluff) transparent = true; else transparent = false; // a default thing
		}
	
		public char ReturnCharacter(){
			return glyph.character;
		}
		
	}
	
	
	
}
