﻿
using System;

namespace Roguelike_Project
{
	
	
	/// <summary>
	/// Description of Messages.
	/// </summary>
	/// 
	public class MessageConsole
	{
		public const int MSG_LIMIT = 100;
		public Message[] messages;
		
		public MessageConsole()
		{
			messages = new Message[MSG_LIMIT];
			for(int i=0;i<MSG_LIMIT;i++){ messages[i] = new Message("",0, Colour.ByName(ColourName.GUIDefault) ,true); } // pre-set array
		}
		
		public void AddMessage(Message msg)
		{
			Message[] tempMSG = messages;
			for (int i = messages.Length-1; i >= 0; i--) {
				if(i+1<tempMSG.Length) tempMSG[i+1] = messages[i];
			}
			tempMSG[0] = msg;
			messages = tempMSG;
			return;
		}
	
	}

	public static class MSGH // Messagehelper
	{
		public static void Print(string m, Colour colour)
		{
			gameManager.Instance.msgConsole.AddMessage(new Message(m,(uint)GTime.Get().TURN, colour));
		}
		public static void Print(string m)
		{
			gameManager.Instance.msgConsole.AddMessage(new Message(m,(uint)GTime.Get().TURN, Colour.ByName(ColourName.GUIDefault)));
		}
		
		public static Message Get(int index)
		{
			if(index<0) index = 0; 
			else if(index>=100) index = 99;
			return gameManager.Instance.msgConsole.messages[index];
		}
		
	}
	
	public struct Message
	{
		public string MSG;
		public uint TurnStamp;
		public Colour colour;
		public bool visible;
		
		public Message(string msg, uint turn, Colour col, bool Empty = false)
		{
			MSG = msg; TurnStamp = turn; colour = col; visible = Empty;
		}
	}
}
