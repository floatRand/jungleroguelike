﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	public enum AttackDescriptor{ Slash, Blunt, Pierce, Fist }
	public enum AttackElement{ Normal, Venom, Ice, Heat, Elec }
	public static class Desciptors
	{
	
		public static string GetWoundString(int curhp, int maxhp)
		{
			float ratio = ((float)curhp)/((float)maxhp);
			
			if(ratio>=1.0f) return "unharmed";
			if(ratio>=0.9f) return "merely scratched";
			if(ratio>=0.8f) return "harmed";
			if(ratio>=0.7f) return "slightly wounded";
			if(ratio>=0.6f) return "wounded";
			if(ratio>=0.5f) return "wounded";
			if(ratio>=0.4f) return "severely wounded";
			if(ratio>=0.3f) return "seriously wounded";
			if(ratio>=0.2f) return "gravely wounded";
			if(ratio>=0.1f) return "mortally wounded";
			return "on brink of death";
		}
		public static Colour GetWoundColour(int curhp, int maxhp)
		{
			float ratio = ((float)curhp)/((float)maxhp);
			
			if(ratio>=1.0f) return new Colour(0,255,0);
			if(ratio>=0.8f) return new Colour(60,200,0);
			if(ratio>=0.6f) return new Colour(255,255,0);
			if(ratio>=0.4f) return new Colour(255,127,0);
			if(ratio>=0.2f) return new Colour(255,0,0);
		    return new Colour(155,0,0);
		}
			/**#################################################################################################**/
 			/**######################################## attack descri ##########################################**/
			/**#################################################################################################**/
		public static string DescribeAttack(AttackDescriptor atkD, string weaponName, CreatureBase attacker, CreatureBase defender, bool finisher = false )
		{
		string res = "Buggy things happens, check DescribeAttack";
		int rng = RNG.GetValue(1,4);
		if(!defender.IsPlayer && attacker.IsPlayer) /** PLAYER ATTACKS! **/
		{
			switch(atkD){
				case AttackDescriptor.Blunt:
					if(rng==1) res = "You bash "+defender.name+" with your "+weaponName+".";
					if(rng==2) res = "You pound "+defender.name+" with your "+weaponName+".";
					if(rng==3) res = "You smash "+defender.name+" with your "+weaponName+".";
					if(rng==4) res = "You beat "+defender.name+" like a bongo with your "+weaponName+".";
					if(finisher) res = "You crush "+defender.name+" with your "+weaponName+".";
						return res;
				case AttackDescriptor.Pierce:
					if(rng==1) res = "You sting "+defender.name+" with your "+weaponName+".";
					if(rng==2) res = "You pierce "+defender.name+" with your "+weaponName+".";
					if(rng==3) res = "You poke "+defender.name+" with your "+weaponName+".";
					if(rng==4) res = "You make shishkebab out of "+defender.name+".";
					if(finisher) res = "You impale "+defender.name+".";
						return res;
				case AttackDescriptor.Slash:
					if(rng==1) res = "You slice "+defender.name+" with your "+weaponName;
					if(rng==2) res = "You slash at "+defender.name+" with your "+weaponName;
					if(rng==3) res = "You cut "+defender.name+" with your "+weaponName;
					if(rng==4) res = "You shear "+defender.name+" like cloth with your "+weaponName;
					if(finisher) res = "You deliver finishing blow to "+defender.name+" with your "+weaponName;
						return res;
				case AttackDescriptor.Fist: // initiate FUN mode
					if(rng==1) res = "You deliver a devastating punch to "+defender.name+".";
					if(rng==2) res = defender.name+" is on the receiving end of your mighty kick!";
					if(rng==3) res = "You punch "+defender.name+" repeatedly, ora ora!";
					if(rng==4) res = defender.name+" is pummeled!";
					if(finisher) res = defender.name+" is already dead.";
						return res;
				default: return res;
			}
		}
		if(defender.IsPlayer && !attacker.IsPlayer) /** ENEMY! **/
		{
			switch(atkD){
				case AttackDescriptor.Blunt:
					    res = attacker.name+" bashes you with "+weaponName+".";;
						return res;
				case AttackDescriptor.Pierce:
						res = attacker.name+" cuts you with "+weaponName+".";;
						return res;
				case AttackDescriptor.Slash:
						res = attacker.name+" stings you with "+weaponName+".";;
						return res;
				case AttackDescriptor.Fist: 
					    res =  attacker.name+" punches you with "+weaponName+".";;
						return res;
				default: return res;
			}
		}
		return res;
		}
	/**#################################################################################################**/
 	/**################################################## end ##########################################**/
	/**#################################################################################################**/
		public static string GenerateRandomSillyName()
		{
			string res = "";
			
			res+=RNG.SelectRandom(new String[]{
			                      "Friendly", "Strong", "Mean", "Ominous", "Funny", "Exciting", "Big", "Hostile", // adjectives
			                      "Purple","Red","Green","Blue", "Black", "White", "Cyan", "Colourful", "Genuine",// colours
			                      "Festive","Flamboyant", "Dull", "Furry", "Scaly", "Gargantuan", "Sick", "Ill", "Radical", "Radiating", "Divine", "Unholy"	
			                      })+" ";
			res+=RNG.SelectRandom(new String[]{
			                      "cloud", "slice of bread", "dragon", "man", "shape", "turtle", "mushroom", "angel", "demon", "horror", "moon troll",
			                      "otter", "dodo", "banana", "mountain dwarf", "sludge elf", "spider", "lizard", "mammal", "skeleton", "clown", "face",
			                      "bird", "insect", "hole", "balloon", "snake", "puddle", "spore", "forest god"
			                      });
			return res;
			
		}
	}
	
	public enum GPron{ g, s, o }; // genetive, subjective, objective
	public static class Gwords{
	
		
		public static Dictionary<CreatureGender,string> genetives = new Dictionary<CreatureGender,string>()
		{
		{CreatureGender.female,"her"},
		{CreatureGender.male,"his"},
		{CreatureGender.other,"its"}
		};
		public static Dictionary<CreatureGender,string> subjectPronouns = new Dictionary<CreatureGender,string>()
		{
		{CreatureGender.female,"she"},
		{CreatureGender.male,"he"},
		{CreatureGender.other,"it"}
		};	
		public static Dictionary<CreatureGender,string> objectPronouns = new Dictionary<CreatureGender,string>()
		{
		{CreatureGender.female,"her"},
		{CreatureGender.male,"him"},
		{CreatureGender.other,"it"}
		};	
		
	}
	
	
}
