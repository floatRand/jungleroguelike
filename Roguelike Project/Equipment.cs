﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Equipment.
	/// </summary>
	public class Equipment
	{
		
		public List<EquipmentSlot> slots;
		public bool melded;
		public CreatureBase holder;
		
		public Equipment(CreatureBase h)
		{
			holder = h;
			slots = new List<EquipmentSlot>();
		}
		
		public void AutoEquipItem(Inv_Slot invSlot){
			ItemBase theItem = invSlot.GetSample(); 
			bool slotExists = slots.Exists( elm => (elm.empty && elm.equipType == theItem.equipSlot) );
			if(isAlreadyEquipped(theItem) || theItem.equipped){ MSGH.Print(theItem.name+" is already equipped."); return; }
			if(slotExists && theItem!=null){ 
				EquipmentSlot theSlot = slots.Find( elm => (elm.empty && elm.equipType == theItem.equipSlot) );
				theSlot.Equip(invSlot.TakeItem(),theSlot.equipType);
				MSGH.Print("You equip "+theItem.name);
				return;
			}
			else{  
				MSGH.Print("There was no good open slot for "+theItem.name);
				return;
			}
		}
		
		public void UnEquip(EquipmentSlot invSlot, Inventory inv)
		{
			bool IsFull = inv.HasSpace();
			if(!invSlot.empty)
			{
				if(!IsFull) inv.AddItem(invSlot.Unequip(false));
				else{ invSlot.Unequip(true); }
			}
		
		}
		
		public bool isAlreadyEquipped(ItemBase it)
		{
			foreach(EquipmentSlot eq in slots)
			{
				if(eq.GetItem() == it ) return true;
			}
			return false;
		}
		public void EjectEquipment(){}
		public void DropEquipment(EquipmentSlot equi){}
		public void ProcessEquipEffects(CreatureBase holder){ foreach(EquipmentSlot slot in slots){ if(slot.GetItem()!=null) slot.GetItem().Worn_Effect(holder); } }
	}
	
	
	public class EquipmentSlot
	{
		public EquipSlot equipType;
		protected ItemBase item;
		public bool empty;	
		public CreatureBase holder;
		public char key;
		
		public EquipmentSlot(EquipSlot equipT, CreatureBase h)
		{
			equipType = equipT;
			empty = true; item = null;
			holder = h;
			key = 'a';
		}
		
		public ItemBase GetItem(){return item;}
		
		public void Equip(ItemBase it, EquipSlot eqSlot){
			item = it; empty = false;
			item.equipped = true;
			item.equippedOn = eqSlot;
			item.Equip_Effect(holder);
		}
		
		public ItemBase Unequip(bool ejectFromInventory=false){
			item.Unequip_Effect(holder);
			item.equipped = false;
			item.equippedOn = EquipSlot.none;
			empty = true; 
			ItemBase itemReturn = item; item = null;
			return itemReturn;
		}
		
		public void InvokeWearEffect(){
			item.Worn_Effect(holder);
		}
		
	}
	
	
	
}
