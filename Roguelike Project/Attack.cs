﻿
using System;

namespace Roguelike_Project
{
	
	public struct Attack
	{
		public Die damageDie;
		public int accuracy, damageBonus, DRpierce;
		public bool completelyPierceDR;
		public BrandBase brand;
		public string name;
		public float strCf, dexCf;
		public AttackDescriptor attackDescriptor;
		
		public Attack(Die die, BrandBase dmgBrand, string nam, AttackDescriptor attackDesc, 
		              float STRcoEff = 0.5f, float DEXcoEFF = 0.5f,
		              int accBonus = 0, int dmgBonus = 0, int DR_pierce_value = 0){
			
			damageDie = die; name = nam;  attackDescriptor = attackDesc;
			brand = dmgBrand; accuracy = accBonus; damageBonus = dmgBonus; DRpierce = DR_pierce_value;
			completelyPierceDR = false; strCf = STRcoEff; dexCf = DEXcoEFF;
		}
	}
	
	public class BrandBase{ public virtual void AlterAttackStats(Attack atk){} public virtual void AttackEffect(CreatureBase attacker, CreatureBase defender){} }
	
	public class Brand_Vanilla : BrandBase{ } // Vanilla
	
	
}
