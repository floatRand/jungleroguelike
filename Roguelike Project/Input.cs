﻿using System;
using System.Collections.Generic;
using SFML.Window;
namespace Roguelike_Project
{
	public static class Input
	{
		public static bool IsKeyPressed(char k){ return gameManager.Instance.inputManager.ContainsKey(k); }
		public static char GetAKey(){ return gameManager.Instance.inputManager.keysPressed.Find( elm => (elm!=null)); }
		public static bool IsAnyKeyPressed(){ return gameManager.Instance.inputManager.keysPressed.Count!=0; }
		public static void EnteredText(object sender, TextEventArgs e){gameManager.Instance.inputManager.AddKey(e.Unicode[0]);}
		public static void ClearKeys(){gameManager.Instance.inputManager.ClearKeys();}
	}
	
	public class InputManager
	{
		public List<char> keysPressed;
		public InputManager(){keysPressed = new List<char>();}
		public void ClearKeys(){ keysPressed.Clear(); }
		public bool ContainsKey(char k){ return keysPressed.Contains(k); }
		public void AddKey(char k){ keysPressed.Add(k);}
	}
}
