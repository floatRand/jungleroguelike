﻿
using System;

namespace Roguelike_Project
{
	
	/**#################################################################################################**/
 	/**######################################### L I C H ###############################################**/
	/**#################################################################################################**/
	class Template_Lich : CTemplate
	{
		public Template_Lich()
		{	
			Init(); // required
			Priority = 100;
			rName = "Lich";  rNamePlural = "Liches";
			newType = CreatureType.Unliving; OverrideType = true;
			MagicAffinityOverride = 7; // greater affinity magic than normally for most races. 
									   // Beware of human liches with dreadful 10 affinity
		
		}
		override protected void AddFlags(CreatureBase creature)
		{
			creature.AddcFlag(CreatureFlag.IgnoresNeeds);
		}
	}
	/**#################################################################################################**/
	
}
