﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Prompts.
	/// </summary>
	public static class Prompts
	{
		public static bool BoolPrompt( string question, ColourName promptColour = ColourName.GUIDefault)
		{	
			
			GlyphRenderer promptRender = new GlyphRenderer( 56,20, gameManager.Instance.mainCam );
			promptRender.posY = 28;
			GUIStuff.CreateAGUIBox_Twoline(promptRender,1,1,55,8);
			promptRender.AddText("="+question+"=",28-(question.Length+2)/2,3, promptColour );
			promptRender.AddText("(y)es       /       (n)o", 25-(9), 5 );
			
		
			while(true){
			Input.ClearKeys();
			gameManager.Instance.window.DispatchEvents();	
			if(Input.IsKeyPressed('y') || Input.IsKeyPressed('Y')) return true;
			if(Input.IsKeyPressed('n') || Input.IsKeyPressed('N')) return false;
			promptRender.UpdateArray();
			gameManager.Instance.window.Draw(promptRender.asciiRenderer);
			gameManager.Instance.window.Display();
			
			}
		}
		
		public static int IntPrompt( string question, ColourName promptColour = ColourName.GUIDefault)
		{	
			int num=0;
			GlyphRenderer promptRender = new GlyphRenderer( 56,20, gameManager.Instance.mainCam );
			promptRender.posY = 28;
			GUIStuff.CreateAGUIBox_Twoline(promptRender,1,1,55,8);
			promptRender.AddText("="+question+"=",28-(question.Length+2)/2,3, promptColour );
			promptRender.AddText("(y)es       /       (n)o", 25-(9), 5 );
			
		
			while(true){
			Input.ClearKeys();
			gameManager.Instance.window.DispatchEvents();	
			
			if(Input.IsKeyPressed('0')) num = ( num * 10 ) + 0;
			if(Input.IsKeyPressed('1')) num = ( num * 10 ) + 1;
			if(Input.IsKeyPressed('2')) num = ( num * 10 ) + 2;
			if(Input.IsKeyPressed('3'))	num = ( num * 10 ) + 3;	 				
			if(Input.IsKeyPressed('4'))	num = ( num * 10 ) + 4;
			if(Input.IsKeyPressed('5'))	num = ( num * 10 ) + 5;
			if(Input.IsKeyPressed('6'))	num = ( num * 10 ) + 6;
			if(Input.IsKeyPressed('7'))	num = ( num * 10 ) + 7;
			if(Input.IsKeyPressed('8')) num = ( num * 10 ) + 8;
			if(Input.IsKeyPressed('9')) num = ( num * 10 ) + 9;
												
			if(Input.IsKeyPressed('y') || Input.IsKeyPressed('Y')) return num;

			promptRender.UpdateArray();
			gameManager.Instance.window.Draw(promptRender.asciiRenderer);
			gameManager.Instance.window.Display();
			
			}
		}
		
		public static int[] pointTargeter( bool tracer )
		{	
			int[] targetPos = new int[]{ gameManager.Instance.player.x,  gameManager.Instance.player.y };
			
			GlyphRenderer promptRender = new GlyphRenderer( 32,32, gameManager.Instance.mainCam );
		
			while(true){
			Input.ClearKeys();
			gameManager.Instance.window.DispatchEvents();	
			promptRender.ResetGlyphMap(true);
				promptRender.glyphMap = (Glyph[,]) gameManager.Instance.mainRend.glyphMap.Clone();
				promptRender.posX = 1; promptRender.posY = 1;
				GUIStuff.CreateAGUIBox_Twoline( promptRender, 0,0,27,27,true);
						if(Input.IsKeyPressed('8')){ targetPos[1]--; }
						if(Input.IsKeyPressed('2')){ targetPos[1]++; }
						if(Input.IsKeyPressed('4')){ targetPos[0]--; }
						if(Input.IsKeyPressed('6')){ targetPos[0]++; }

						if(Input.IsKeyPressed('7')){ targetPos[0]--; targetPos[1]--; }
						if(Input.IsKeyPressed('9')){ targetPos[0]++; targetPos[1]--; }
						if(Input.IsKeyPressed('1')){ targetPos[0]--; targetPos[1]++;  }
						if(Input.IsKeyPressed('3')){ targetPos[0]++; targetPos[1]++;  }
						
						if(targetPos[0] > gameManager.Instance.player.x+12) targetPos[0] = gameManager.Instance.player.x+12;
						if(targetPos[0] < gameManager.Instance.player.x-12) targetPos[0] = gameManager.Instance.player.x-12;
						if(targetPos[1] > gameManager.Instance.player.y+12) targetPos[1] = gameManager.Instance.player.y+12;
						if(targetPos[1] < gameManager.Instance.player.y-12) targetPos[1] = gameManager.Instance.player.y-12;
						
			if(Input.IsKeyPressed('y') || Input.IsKeyPressed('t')) return targetPos;
			promptRender.InsertGlyph(targetPos[0],targetPos[1],new Glyph('X'),true);
			promptRender.UpdateArray();
			gameManager.Instance.window.Draw(promptRender.asciiRenderer);
			gameManager.Instance.window.Display();
			
			}
		}
		
		public static CreatureBase creatureTargeter( Map currentMap, bool tracer )
		{	
			int[] targetPos = new int[]{ gameManager.Instance.player.x,  gameManager.Instance.player.y };
			
			GlyphRenderer promptRender = new GlyphRenderer( 32,32, gameManager.Instance.mainCam );
		
			while(true){
			Input.ClearKeys();
			gameManager.Instance.window.DispatchEvents();	
			promptRender.ResetGlyphMap(true);
				promptRender.glyphMap = (Glyph[,]) gameManager.Instance.mainRend.glyphMap.Clone();
				promptRender.posX = 1; promptRender.posY = 1;
				GUIStuff.CreateAGUIBox_Twoline( promptRender, 0,0,27,27,true);
						if(Input.IsKeyPressed('8')){ targetPos[1]--; }
						if(Input.IsKeyPressed('2')){ targetPos[1]++; }
						if(Input.IsKeyPressed('4')){ targetPos[0]--; }
						if(Input.IsKeyPressed('6')){ targetPos[0]++; }

						if(Input.IsKeyPressed('7')){ targetPos[0]--; targetPos[1]--; }
						if(Input.IsKeyPressed('9')){ targetPos[0]++; targetPos[1]--; }
						if(Input.IsKeyPressed('1')){ targetPos[0]--; targetPos[1]++;  }
						if(Input.IsKeyPressed('3')){ targetPos[0]++; targetPos[1]++;  }
						
						if(targetPos[0] > gameManager.Instance.player.x+12) targetPos[0] = gameManager.Instance.player.x+12;
						if(targetPos[0] < gameManager.Instance.player.x-12) targetPos[0] = gameManager.Instance.player.x-12;
						if(targetPos[1] > gameManager.Instance.player.y+12) targetPos[1] = gameManager.Instance.player.y+12;
						if(targetPos[1] < gameManager.Instance.player.y-12) targetPos[1] = gameManager.Instance.player.y-12;
						
						if((Input.IsKeyPressed('y') || Input.IsKeyPressed('t')) && currentMap.IsCreatureOnTile(targetPos[0],targetPos[1]))
							return currentMap.GetCreatureOnTile(targetPos[0],targetPos[1]);
						
			promptRender.InsertGlyph(targetPos[0],targetPos[1],new Glyph('X'),true);
			promptRender.UpdateArray();
			gameManager.Instance.window.Draw(promptRender.asciiRenderer);
			gameManager.Instance.window.Display();
			
			}
		}
		
		
		
		
	}
}
