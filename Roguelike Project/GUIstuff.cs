﻿
using System;
using System.Collections.Generic;
using SFML.Window;
namespace Roguelike_Project
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public static class GUIStuff
	{
		/**#################################################################################################**/
 		/**####################################### PLAYERINFO ##############################################**/
		/**#################################################################################################**/
		public static void GeneratePlayerInfo(GlyphRenderer GUIGlyphRenderer, Player player)
		{
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,28,1,22,10);
				//PLAYERINFO
				GUIGlyphRenderer.AddText(player.name,29,2);
				GUIGlyphRenderer.AddText("Lv"+player.HD+" "+player.raceName,29,3);
				GUIGlyphRenderer.AddText(player.currentHP+"/"+player.maxHP+"HP",2+24,28);
				GUIStuff.CreateAGUI_Healthbar( GUIGlyphRenderer, 2,28,player.currentHP,player.maxHP,24, (char)219 );
				GUIGlyphRenderer.AddText(player.currentEP+"EP",7+21,27);
			    GUIStuff.CreateAGUI_EPbar( GUIGlyphRenderer, 7+21,26,player.currentEP,player.maxEP,16, (char)219 );
			    TimeFormat timeForm = GTime.Get().GetTimeFormat();
			    GUIGlyphRenderer.AddText("Turn: "+(float)(GTime.Get().TURN),41,27);
			    GUIGlyphRenderer.AddText("Time: "+timeForm.GetDayHours(),41,28);
			    GUIGlyphRenderer.AddText("Date: "+timeForm.GetString(),20,28+13);
				 
				//stats
				GUIGlyphRenderer.AddText("STR  "+player.GetAbilityScore(AbilityScore.strength),30,4);
				GUIGlyphRenderer.AddText("DEX  "+player.GetAbilityScore(AbilityScore.dexterity),30,5);
				GUIGlyphRenderer.AddText("FORT "+player.GetAbilityScore(AbilityScore.fortitude),30,6);
				
				GUIGlyphRenderer.AddText("INT  "+player.GetAbilityScore(AbilityScore.intelligence),39,4);
				GUIGlyphRenderer.AddText("WILL "+player.GetAbilityScore(AbilityScore.will),39,5);
				GUIGlyphRenderer.AddText("PER  "+player.GetAbilityScore(AbilityScore.fortitude),39,6);

				GUIGlyphRenderer.AddText("EV "+player.EV,30,8);
				GUIGlyphRenderer.AddText("DR "+player.DR,30,9);
				
				// RECENT TARGET
				player.TestDropRecentTarget(); // check if enemy is in 1. Out of FOV 2. Dead. If so, drop target since it is not very interesting. 
				if(player.recentTarget!=null){
						GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,29,11,20,5, true);
						GUIGlyphRenderer.InsertGlyph(30, 12, player.recentTarget.glyph);
					 	GUIGlyphRenderer.AddText(player.recentTarget.name, 32, 12 );
					 	GUIStuff.CreateAGUI_Healthbar( GUIGlyphRenderer, 32,13,player.recentTarget.currentHP,player.recentTarget.maxHP,12, (char)219 );
				}
		}
		/**#################################################################################################**/
 		/**########################################## MSG ##################################################**/
		/**#################################################################################################**/
		public static void GenerateMSGBox(GlyphRenderer GUIGlyphRenderer)
		{
				//MGSBOX
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,29,55,12);
				for(int i=0;i<10;i++){
					GUIGlyphRenderer.AddText(MSGH.Get(i).MSG,2,39-i,MSGH.Get(i).colour, new Colour(0,0,0));
				}
		}
		/**#################################################################################################**/
		/**#################################### CURSORMODE #################################################**/
		/**#################################################################################################**/
		public static void GenerateCursorViewBox(GlyphRenderer GUIGlyphRenderer, Map map, int x0, int y0)
		{
			CreatureBase targ;
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,29,54,12);
				GUIGlyphRenderer.InsertGlyph(x0,y0,new Glyph(0,(char)206,Colour.ByName(ColourName.GUIDefault), new Colour(0,0,0)), true);
				if(x0>0 && y0>0 
					   && x0<map.mapdata.GetLength(0) 
					   && y0<map.mapdata.GetLength(1)){
					
				targ = map.GetCreatureOnTile(x0,y0);
					if(targ!=null)
					{
						GUIGlyphRenderer.AddText("v - "+targ.name+" ("+Desciptors.GetWoundString(targ.currentHP,targ.maxHP)+")", 3, 31, Desciptors.GetWoundColour(targ.currentHP,targ.maxHP), new Colour(0,0,0));
						if(targ.currentAction!=null) if(targ.currentAction.state==ActionState.Pending) GUIGlyphRenderer.AddText(targ.currentAction.descripe, 4, 32);
					}
				}
		}
		/**#################################################################################################**/
 		/**######################################## FOV LIST ###############################################**/
		/**#################################################################################################**/
		public static void GenerateFOVEnemyList(GlyphRenderer GUIGlyphRenderer, List<CreatureBase> creaturesInFOVList, bool displacedByTargetView = false)
		{
				int duud = 0;
				int dY = 0;
				if(!displacedByTargetView) dY = 5;
				GUIGlyphRenderer.AddText("-Creatures in sight-", 30, 16-dY );
				
				CollapseableObjects collapseCreatures = new CollapseableObjects();
				
				foreach(CreatureBase elm in creaturesInFOVList)
				{
					collapseCreatures.Process(elm.name, elm.raceNamePlural, elm.glyph);
				}
				foreach(CollapseableObj elm in collapseCreatures.objs)
				{
					if(duud<8){
					GUIGlyphRenderer.InsertGlyph(30, 17+duud-dY, elm.glyph);
					GUIGlyphRenderer.AddText(elm.numOfObjects+" "+elm.GetPluralizedName(), 32, 17+duud-dY, ColourName.GUIDefault, ColourName.Transparent);
					duud++;
					}
				}
				if(duud>=8)
				{
					GUIGlyphRenderer.AddText("(...)", 30, 17+duud-dY );
				}
				if(duud!=0) return;
				if(collapseCreatures.objs.Count!=0) collapseCreatures.Nuke();
				GUIGlyphRenderer.AddText("(none)", 30, 17+duud-dY );
		}
		
		/**#################################################################################################**/
 		/**######################################## OBSERVE ################################################**/
		/**#################################################################################################**/
		public static void GenerateObserveWindow(GlyphRenderer GUIGlyphRenderer, List<CreatureBase> creaturesInFOVList, List<ItemBase> iteminLos)
		{
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,54,40);
				GUIGlyphRenderer.AddText("-=Creatures in field of vision=-", 2, 2 );
				int duud = 0;
				int[] lastpos = new int[]{0,0};
				foreach(CreatureBase elm in creaturesInFOVList)
				{
					if(duud<32){
					GUIGlyphRenderer.InsertGlyph(3, 4+duud, elm.glyph);
					if(elm.currentAction.state==ActionState.Pending) lastpos=GUIGlyphRenderer.AddText(elm.name+", "+elm.currentAction.descripe+", ", 5, 4+duud, ColourName.GUIDefault, ColourName.Transparent);
					else lastpos=GUIGlyphRenderer.AddText(elm.name+" ", 5, 4+duud, ColourName.GUIDefault, ColourName.Transparent);
					GUIGlyphRenderer.AddText(Desciptors.GetWoundString(elm.currentHP,elm.maxHP), lastpos[0]+5, 4+duud, Desciptors.GetWoundColour(elm.currentHP,elm.maxHP), new Colour(0,0,0));
					duud++;
					}
					if(duud>=32)
					{
					GUIGlyphRenderer.AddText("(...)", 6, 4+duud);
					}
				}
				GUIGlyphRenderer.AddText("-=Items in field of vision=-", 2, 5+duud );
				foreach(ItemBase elm in iteminLos)
				{
					if(duud<32){
					GUIGlyphRenderer.InsertGlyph(3, 7+duud, elm.glyph);
					lastpos=GUIGlyphRenderer.AddText(elm.name+" ", 5, 7+duud, ColourName.GUIDefault, ColourName.Transparent);
					duud++;
					}
					if(duud>=32)
					{
					GUIGlyphRenderer.AddText("(...)", 6, 7+duud);
					}
				}
		}
		/**#################################################################################################**/
 		/**####################################### INVENTORY ###############################################**/
		/**#################################################################################################**/
		public static void GenerateInventoryWindow(GlyphRenderer GUIGlyphRenderer, CreatureBase p, char highlight)
		{
			GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40);
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,5,1,47,14);
		
				int duud = 0;
				int totalPages = ( p.inventory.slots.Count / 21 )+1;
				
				if(Input.IsKeyPressed('+') && (p.inventory.page+1)<totalPages){ p.inventory.page++; }
				if(Input.IsKeyPressed('-') && p.inventory.page>0){ p.inventory.page--; }
				
				foreach(Inv_Slot elm in p.inventory.slots)
				{
					int n = duud / 15;
					int n2 = duud / 21;
					bool highlighted;
					if(!elm.empty){
					highlighted = (highlight == elm.key);
						string elmName = elm.GetSample().GetName();
						GUIGlyphRenderer.InsertGlyph(7+duud*3-n*45, 3+n*3, elm.GetSample().glyph);
						ColourName bgCol = ColourName.Transparent; ColourName charCol = ColourName.GUIDefault; ColourName boxCol = ColourName.GUIDefault;
						if(highlighted){bgCol = ColourName.GUIDefault;  charCol = ColourName.Black;}
						if(elm.items[0].equipped){ 
							if(!highlighted) charCol = ColourName.White;
							else bgCol = ColourName.White;
							boxCol = ColourName.White;
							}
						
						
						if(!highlighted)CreateAGUIBox_Oneline(GUIGlyphRenderer,7+duud*3-n*45-1,3+n*3-1,3,3, true, boxCol);
						else{CreateAGUIBox_Twoline(GUIGlyphRenderer,7+duud*3-n*45-1,3+n*3-1,3,3, true, boxCol);}
						
							if(duud<((p.inventory.page+1)*21) && duud>=(p.inventory.page*21)){
						if(!elm.items[0].equipped){
									GUIGlyphRenderer.AddText(elm.key+"-"+elmName, 3, 16+duud-n2*21, charCol, bgCol);}
						else{
								GUIGlyphRenderer.AddText(elm.key+"-"+elmName + " (equipped on "+elm.items[0].equippedOn+")", 3, 16+duud-n2*21, charCol, bgCol);}
							}
						
						
						if(highlighted){
							GUIGlyphRenderer.AddText(elmName + " - "+elm.GetSample().equipSlot+" - "+ elm.GetSample().meleeDmgDie.GetinString(), 5, 38);
						}
							duud++;
					}
				}
				
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40,true);
						GUIGlyphRenderer.AddText("Inventory", 23, 1 );
						GUIGlyphRenderer.AddText("Page "+(p.inventory.page+1)+"/"+totalPages, 45, 39 );
		}
		/**#################################################################################################**/
 		/**####################################### INVENTORY ###############################################**/
		/**#################################################################################################**/
		public static void GenerateDropItemWindow(GlyphRenderer GUIGlyphRenderer, CreatureBase p, char highlight, bool initialPress = false)
		{
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40);
		
				int duud = 0;
				int totalPages = ( p.inventory.slots.Count / 28 )+1;
				
				if(Input.IsKeyPressed('+') && (p.inventory.page+1)<totalPages){ p.inventory.page++; }
				if(Input.IsKeyPressed('-') && p.inventory.page>0){ p.inventory.page--; }
				
				foreach(Inv_Slot elm in p.inventory.slots.FindAll( elm => elm.GetSample().equippable ))
				{
					int n = duud / 15;
					int n2 = duud / 28;
					bool highlighted;
					if(!elm.empty){
					highlighted = (highlight == elm.key);
						string elmName = elm.GetSample().GetName();
						ColourName bgCol = ColourName.Transparent; ColourName charCol = ColourName.GUIDefault; 
						if(highlighted){bgCol = ColourName.GUIDefault;  charCol = ColourName.Black;}
						if(elm.items[0].equipped){ 
							if(!highlighted) charCol = ColourName.White;
							else bgCol = ColourName.White;
							}
						if(duud<((p.inventory.page+1)*28) && duud>=(p.inventory.page*28)){
						if(!elm.items[0].equipped){
									GUIGlyphRenderer.AddText(elm.key+"-"+elmName, 3, 6+duud-n2*21, charCol, bgCol);}
							}
						
						
						if(highlighted){
							GUIGlyphRenderer.AddText(elmName + " - "+elm.GetSample().equipSlot+" - "+ elm.GetSample().meleeDmgDie.GetinString(), 5, 38);
						}
							duud++;
							
							if(Input.IsKeyPressed(elm.key) && !initialPress){ 
								bool stackDrop = false;
								if(elm.items.Count>1){
									
									List<ItemBase> droppedItems = elm.TakeMultItem();
									foreach(ItemBase ite in droppedItems)
									{
										ite.x = p.x; ite.y = p.y;
										ite.onGround = true;
										gameManager.Instance.currentMap.items.Add(ite);
									}
								}
								else{
								ItemBase droppedItem = elm.TakeItem();
								droppedItem.x = p.x; droppedItem.y = p.y;
								droppedItem.onGround = true;
								gameManager.Instance.currentMap.items.Add(droppedItem);
								}
								
								gameManager.Instance.menuState = MenuState.none; 
								break;
							}
					}
					
				}
				
				
						GUIGlyphRenderer.AddText("Drop item", 23, 1 );
						GUIGlyphRenderer.AddText("Page "+(p.inventory.page+1)+"/"+totalPages, 45, 39 );
		}
		/**#################################################################################################**/
 		/**########################################## WEAR #################################################**/
		/**#################################################################################################**/
		public static void GenerateWearItemWindow(GlyphRenderer GUIGlyphRenderer, CreatureBase p, char highlight, bool initialPress = false)
		{
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40);
		
				int duud = 0;
				int totalPages = ( p.inventory.slots.Count / 28 )+1;
				
				if(Input.IsKeyPressed('+') && (p.inventory.page+1)<totalPages){ p.inventory.page++; }
				if(Input.IsKeyPressed('-') && p.inventory.page>0){ p.inventory.page--; }
				
				foreach(Inv_Slot elm in p.inventory.slots.FindAll( elm => elm.GetSample().equippable ))
				{
					int n = duud / 15;
					int n2 = duud / 28;
					bool highlighted;
					if(!elm.empty){
					highlighted = (highlight == elm.key);
						string elmName = elm.GetSample().GetName();
						ColourName bgCol = ColourName.Transparent; ColourName charCol = ColourName.GUIDefault; 
						if(highlighted){bgCol = ColourName.GUIDefault;  charCol = ColourName.Black;}
						if(elm.items[0].equipped){ 
							if(!highlighted) charCol = ColourName.White;
							else bgCol = ColourName.White;
							}
						if(duud<((p.inventory.page+1)*28) && duud>=(p.inventory.page*28)){
						if(!elm.items[0].equipped){
									GUIGlyphRenderer.AddText(elm.key+"-"+elmName, 3, 6+duud-n2*21, charCol, bgCol);}
							}
						
						
						if(highlighted){
							GUIGlyphRenderer.AddText(elmName + " - "+elm.GetSample().equipSlot+" - "+ elm.GetSample().meleeDmgDie.GetinString(), 5, 38);
						}
							duud++;
							
							if(Input.IsKeyPressed(elm.key) && !initialPress){ 
								p.equipment.AutoEquipItem( elm );
								gameManager.Instance.menuState = MenuState.none; 
								break;
							}
					}
					
				}
				
				
						GUIGlyphRenderer.AddText("Wear/wield item", 23, 1 );
						GUIGlyphRenderer.AddText("Page "+(p.inventory.page+1)+"/"+totalPages, 45, 39 );
		}
		/**#################################################################################################**/
 		/**########################################## UNEQUIP ##############################################**/
		/**#################################################################################################**/
			public static void GenerateUnequipItemWindow(GlyphRenderer GUIGlyphRenderer, CreatureBase p, char highlight, bool initialPress = false)
		{
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40);
		
				int duud = 0;
				int totalPages = ( p.inventory.slots.Count / 28 )+1;
				
				if(Input.IsKeyPressed('+') && (p.inventory.page+1)<totalPages){ p.inventory.page++; }
				if(Input.IsKeyPressed('-') && p.inventory.page>0){ p.inventory.page--; }
				foreach(EquipmentSlot elm in p.equipment.slots)
				{
					int n = duud / 15;
					int n2 = duud / 28;
					bool highlighted;
					if(!elm.empty){
					highlighted = (highlight == elm.key);
						string elmName = elm.GetItem().name;
						ColourName bgCol = ColourName.Transparent; ColourName charCol = ColourName.GUIDefault; 
						if(highlighted){bgCol = ColourName.GUIDefault;  charCol = ColourName.Black;}
						if(duud<((p.inventory.page+1)*28) && duud>=(p.inventory.page*28)){
							GUIGlyphRenderer.AddText(elm.key+" - "+elm.equipType.ToString()+": "+elmName, 3, 6+duud-n2*21, charCol, bgCol);
							}
						
						if(highlighted){
							GUIGlyphRenderer.AddText(elmName + elm.GetItem().meleeDmgDie.GetinString(), 5, 38);
						}
							duud++;
							
							if(Input.IsKeyPressed(elm.key) && !initialPress){ 
								p.equipment.UnEquip( elm, p.inventory );
								gameManager.Instance.menuState = MenuState.none; 
								MSGH.Print("You unequip " + elmName);
								break;
							}
					}
					
				}
				if(duud==0){ GUIGlyphRenderer.AddText("You are completely naked.", 5, 7); }
				
				
						GUIGlyphRenderer.AddText("Unequip/unwield item", 18, 1 );
					//	GUIGlyphRenderer.AddText("Page "+(p.inventory.page+1)+"/"+totalPages, 45, 39 );
		}
		/**#################################################################################################**/
 		/**######################################## ABILITY ################################################**/
		/**#################################################################################################**/
		public static void GenerateAbilityWindow(GlyphRenderer GUIGlyphRenderer, CreatureBase p)
		{
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40);
				GUIGlyphRenderer.AddText("Abilities", 2, 2 );
				int duud = 0;
			
				foreach(Ability elm in p.abilities)
				{
					GUIGlyphRenderer.AddText(elm.key+" - "+elm.fullName+" - Fail%: "+elm.ChanceofFailure(p)+" - Pow: "+elm.CalculatePower(p)
					                         ,3, 6+duud, ColourName.GUIDefault, ColourName.Transparent); duud++;
				}
		}
		/**#################################################################################################**/
 		/**######################################## DURATIONS ##############################################**/
		/**#################################################################################################**/
		public static void DisplayDurations(GlyphRenderer GUIGlyphRenderer, CreatureBase p)
		{
			int duud = 0;
				foreach(DurationBase elm in p.durations)
				{
					GUIGlyphRenderer.AddText(elm.shortName,50, 1+duud, elm.colour, new Colour(0,0,0,0)); duud++;
				}
		}
		/**#################################################################################################**/
 		/**######################################## CHARVIEW ###############################################**/
		/**#################################################################################################**/
		public static void GenerateCharView(GlyphRenderer GUIGlyphRenderer, Player player)
		{
		
				GUIStuff.CreateAGUIBox_Twoline(GUIGlyphRenderer,1,1,55,40);
				GUIGlyphRenderer.AddText(player.name,2,2);
				GUIGlyphRenderer.AddText("Lv"+player.HD+" "+player.raceName,2,3);
				GUIGlyphRenderer.AddText(player.currentHP+"/"+player.maxHP+"HP "+player.currentEP+"/"+player.maxEP+"EP",2,4);
				GUIGlyphRenderer.AddText("STR  "+player.GetAbilityScore(AbilityScore.strength),4,7);
				GUIGlyphRenderer.AddText("DEX  "+player.GetAbilityScore(AbilityScore.dexterity),4,8);
				GUIGlyphRenderer.AddText("FORT "+player.GetAbilityScore(AbilityScore.fortitude),4,9);
				GUIGlyphRenderer.AddText("INT  "+player.GetAbilityScore(AbilityScore.intelligence),15,7);
				GUIGlyphRenderer.AddText("WILL "+player.GetAbilityScore(AbilityScore.will),15,8);
				GUIGlyphRenderer.AddText("PER  "+player.GetAbilityScore(AbilityScore.fortitude),15,9);
				GUIGlyphRenderer.AddText("EV "+player.EV,4,11);
				GUIGlyphRenderer.AddText("DR "+player.DR,15,11);
				int duud = 0; int[] lastpos = new int[2]; int weaponNumer = 0;
				foreach(EquipmentSlot elm in player.equipment.slots) // equipment
				{
					
					
					
					if(elm.equipType!=EquipSlot.Weapon && elm.equipType!=EquipSlot.Other){ // regular armour
					if(!elm.empty){
					lastpos = GUIGlyphRenderer.AddText(elm.equipType.ToString()+":", 5, 17+duud, ColourName.White, ColourName.Transparent);
					GUIGlyphRenderer.AddText(elm.GetItem().name, 16, 17+duud, ColourName.GUIDefault, ColourName.Transparent);
					GUIGlyphRenderer.InsertGlyph(14, 17+duud, elm.GetItem().glyph);
					}
					else{  
							lastpos = GUIGlyphRenderer.AddText(elm.equipType.ToString()+":", 5, 17+duud, ColourName.White, ColourName.Transparent);
							GUIGlyphRenderer.AddText("(Empty)", 16, 17+duud, ColourName.GUIDefault, ColourName.Transparent);  }
					}
					
					if(elm.equipType == EquipSlot.Weapon)
					{	
						lastpos = GUIGlyphRenderer.AddText(elm.equipType.ToString()+":", 5, 19+duud, ColourName.White, ColourName.Transparent);
						if(elm.empty && weaponNumer < player.mainAttack.Count) 
							GUIGlyphRenderer.AddText(player.mainAttack[weaponNumer].name+" ("+player.mainAttack[weaponNumer].damageDie.GetinString()
							                         +"+"+player.mainAttack[weaponNumer].damageBonus+")", 16, 17+duud+2, ColourName.GUIDefault, ColourName.Transparent);
						else{ GUIGlyphRenderer.AddText(elm.GetItem().name, 16, 17+duud+2, ColourName.GUIDefault, ColourName.Transparent); }
						weaponNumer++;
					}
					
					
					duud++;
					
				}
				
				
				
		}
		
		
		/**#################################################################################################**/
 		/**##################################### GUITRICKS #################################################**/
		/**#################################################################################################**/
		public static void CreateAGUIBox_Twoline(GlyphRenderer rend, int x, int y, int width, int height, bool noFill = false, ColourName colour = ColourName.GUIDefault)
		{
			Colour mainCol = Colour.ByName(colour); 
			for(int i=0; i<width; i++){
				for(int j=0; j<height; j++){			
				
					if((j==0 || j==height-1)) rend.InsertGlyph(x+i,y+j,new Glyph(0,(char)205,mainCol,new Colour(0,0,0))); // =
					else if((i==0 || i==width-1)) rend.InsertGlyph(x+i,y+j,new Glyph(0,(char)186,mainCol,new Colour(0,0,0))); // II
					else if(!noFill) rend.InsertGlyph(x+i,y+j,new Glyph(' '));
					rend.InsertGlyph(x,y, new Glyph(0,(char)201,mainCol,new Colour(0,0,0)));
					rend.InsertGlyph(x+width-1,y, new Glyph(0,(char)187,mainCol,new Colour(0,0,0)));
					rend.InsertGlyph(x,y+height-1, new Glyph(0,(char)200,mainCol,new Colour(0,0,0)));
					rend.InsertGlyph(x+width-1,y+height-1, new Glyph(0,(char)188,mainCol,new Colour(0,0,0)));
					
				}
			}
		}
		
		public static void CreateAGUIBox_Oneline(GlyphRenderer rend, int x, int y, int width, int height, bool noFill = false, ColourName colour = ColourName.GUIDefault)
		{
			Colour mainCol = Colour.ByName(colour); 
			for(int i=0; i<width; i++){
				for(int j=0; j<height; j++){			
				
					if((j==0 || j==height-1)) rend.InsertGlyph(x+i,y+j,new Glyph(0,(char)196,mainCol,new Colour(0,0,0))); // =
					else if((i==0 || i==width-1)) rend.InsertGlyph(x+i,y+j,new Glyph(0,(char)179,mainCol,new Colour(0,0,0))); // II
					else if(!noFill) rend.InsertGlyph(x+i,y+j,new Glyph(' '));
					rend.InsertGlyph(x,y, new Glyph(0,(char)218,mainCol,new Colour(0,0,0))); // LU
					rend.InsertGlyph(x+width-1,y, new Glyph(0,(char)191,mainCol,new Colour(0,0,0))); //RU
					rend.InsertGlyph(x,y+height-1, new Glyph(0,(char)192,mainCol,new Colour(0,0,0))); //LD
					rend.InsertGlyph(x+width-1,y+height-1, new Glyph(0,(char)217,mainCol,new Colour(0,0,0))); //RD
					
				}
			}
		}
		
		public static void CreateAGUI_Healthbar(GlyphRenderer rend, int x, int y, int curhp, int maxhp, int length, char charac = (char)178)
		{
			float ratio = (float)curhp/(float)maxhp;
			for(int i=0; i<length; i++)
			{
				if(i<ratio*length) rend.InsertGlyph(x+i,y, new Glyph(10,charac,new Colour(0,255,0), new Colour(0,0,0))); // GREEN
				if(i>=ratio*length) rend.InsertGlyph(x+i,y, new Glyph(10,charac,new Colour(255,0,0), new Colour(0,0,0))); // RED
			}
		}
		public static void CreateAGUI_EPbar(GlyphRenderer rend, int x, int y, int curhp, int maxhp, int length, char charac = (char)178)
		{
			float ratio = (float)curhp/(float)maxhp;
			for(int i=0; i<length; i++)
			{
				if(i<ratio*length) rend.InsertGlyph(x,y-i, new Glyph(10,charac,new Colour(0,95,240), new Colour(0,0,0))); // BLUE
				if(i>=ratio*length) rend.InsertGlyph(x,y-i, new Glyph(10,charac,new Colour(0,22,100), new Colour(0,0,0))); // DARKBLUE
			}
		}
		
	}
	
	
	
	public class CollapseableObj : IEquatable<CollapseableObj> {
		public string name, pluralname; 
		public int numOfObjects;
		public Glyph glyph;
		public CollapseableObj(string nameofObj, string pluraln, Glyph gly)
			{
			name = nameofObj; numOfObjects = 1; pluralname = pluraln; glyph = gly;
			}
		public CollapseableObj(string nameofObj, string pluraln)
			{
			name = nameofObj; numOfObjects = 1; pluralname = pluraln; glyph = new Glyph();
			}
		
		public void Increment(){ numOfObjects++; }
		public void EmptyContent(){ numOfObjects = 0; name = "Empty Container";}
		public bool Equals(CollapseableObj other){ if(other.name==name) return true; return false;}
		public string GetName(){return name;}
		public string GetPluralizedName(){ if(numOfObjects==1) return name; return pluralname; }
	}
	public class CollapseableObjects
	{
		public List<CollapseableObj> objs;
		public CollapseableObjects()
		{
			objs=new List<CollapseableObj>();
		}
		public void Nuke(){ objs.Clear(); }
		public void Process(string nam, string pluralnam)
		{
			if(!objs.Exists(element => element.GetName()==nam)){objs.Add(new CollapseableObj(nam, pluralnam));}
			else objs.Find(element => element.GetName()==nam).Increment();
		}
		public void Process(string nam, string pluralnam, Glyph gly)
		{
			if(!objs.Exists(element => element.GetName()==nam)){objs.Add(new CollapseableObj(nam, pluralnam, gly));}
			else objs.Find(element => element.GetName()==nam).Increment();
		}
		
	}
	
	
}
