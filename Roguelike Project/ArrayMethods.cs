﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/** Things that are needed to manipulate maps and arrays safely and efficiently, I suppose. **/

	public static class ArrayMethods
	{
		/**#################################################################################################**/
 		/**######################################## Overlay2darray  ########################################**/
		/**#################################################################################################**/
		public static void Overlay2DArray( this Array target, Array layer, int x, int y, Object mask = null )
		{
			if(target.Rank!=2 || layer.Rank!=2){Console.WriteLine("Overlaying failed, not two-dimensional"); return;} // Shit all over this
			if(target.GetType()!=layer.GetType()){Console.WriteLine("Overlaying failed, not same type:"
			                                                        + target.GetType()+"=/=" + layer.GetType()); return;}
				
			for(int i=0;i<layer.GetLength(0);i++){
			for(int j=0;j<layer.GetLength(1);j++){	
			if(i+x>0 && i+x<target.GetLength(0) && 
			   j+y>0 && j+y<target.GetLength(1) )
				{
						if(mask!=null)
						{
							if(!mask.Equals(layer.GetValue(new int[]{i,j})))
							{
								target.SetValue(layer.GetValue(new int[]{i,j}),new int[]{x+i,y+j});
							}
						}
				}
			} // end of j-loop
			} // end of i-loop
			return;
		} /** End of Overlay2DArray() **/
		
		
		/**#################################################################################################**/
 		/**######################################## FLOODFILL ##############################################**/
		/**#################################################################################################**/
		// Use for small things only, otherwise program is die.
		public static void QueueFloodFill( this Array map, int[] pt, object target, object replacement )
		{
			Queue<int[]> q = new Queue<int[]>();
			q.Enqueue(pt);
			
			while(q.Count > 0)
			{
				int[] n = q.Dequeue();
				if( !map.GetValue(n).Equals(target) ) continue;
				int[] w = n, e = new int[]{ n[0]+1, n[1] };
			
			    while((w[0]>0) && map.GetValue(w).Equals(target)){
					map.SetValue(replacement, w);
					if ((w[1] > 0) && map.GetValue(new int[]{w[0],w[1]-1}).Equals(target))
						q.Enqueue(new int[]{w[0], w[1] - 1});
					if ((w[1] < map.GetLength(1)) && map.GetValue(new int[]{w[0],w[1]+1}).Equals(target)) 
                        q.Enqueue(new int[]{w[0], w[1] + 1});
					w[0]--;
		        } 
				while((e[0]<map.GetLength(0)) && map.GetValue(e).Equals(target)){
					map.SetValue(replacement, e);
					if ((e[1] > 0) && map.GetValue(new int[]{e[0],e[1]-1}).Equals(target))
						q.Enqueue(new int[]{e[0], e[1] - 1});
					if ((e[1] < map.GetLength(1)) && map.GetValue(new int[]{e[0],e[1]+1}).Equals(target)) 
                        q.Enqueue(new int[]{e[0], e[1] + 1});
					e[0]++;
		        }
			} // End of while(q.Count >0)
			
		} // End of QueueFloodFill()
	
	
	}/** End oF ArrayMethods **/
	
}