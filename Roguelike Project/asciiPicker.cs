﻿
using System;
using SFML.System;

namespace Roguelike_Project
{
	public struct asciiPicker
	{
		
		public static Vector2f getAsciiCoords(char c){
			int n = (int)c;
			int size = gameManager.Instance.glyphSize;
			int au = n/16;
			int x = ( n*size - au*(size*16) );
			int y = au * size;
			return new Vector2f(x,y);
			
		}
	}
}
