﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of MiscMethod.
	/// </summary>
	public static class MiscMethod
	{
		public static bool IsKeyTurn() // is divisable by 10?
		{
			if(Math.Round(GTime.Get().TURN,2)==0) return true;
			else return false;
		}
		
	}
}
