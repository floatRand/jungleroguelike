﻿
using System;
namespace Roguelike_Project
{
	/// <summary>
	/// Description of Random.
	/// </summary>
	public static class RNG
	{
		public static int GetValue(int minV, int maxV)
		{
			return gameManager.Instance.random.Next(minV,maxV+1);
		}
		
		public static int RollDie( int d, int x )
		{
			int res = 0;
			for(int i=0; i<d; i++){ res = GetValue(1,x);}
			return res;
		}
		
		public static void RandomDistributionTest(int iter, int valueScale)
		{
			int[] results = new int[valueScale+1]; int roll = 0;
			for(int i=0;i<iter;i++){roll = GetValue( 0, valueScale ); results[roll]++; }
			for(int i=0;i<=valueScale;i++){Console.WriteLine("Rolls resulting in "+i+": "+results[i]);}
		}
		
		public static Object SelectRandom(Array objs)
		{
			if(objs.Rank!=1){Console.WriteLine("Use one-dimensional arrays you dolt"); return null;}
			int randomSelection = GetValue(0,objs.GetLength(0)-1);
			return objs.GetValue(new int[]{randomSelection});
		}
		
		public static bool Coinflip(){ return GetValue(0,1)>0; }
		public static bool OneChanceIn(int X){return GetValue(0,X)==0;}
		
	}
	
	public struct Die
	{
		public int d,sides, bonus;	
		public Die(int numberofDie, int numofSides, int b = 0){ d = numberofDie; sides = numofSides; bonus = b; }
		public int Roll(){return RNG.RollDie(d,sides)+bonus;}
		public string GetinString(){ return d+"d"+sides; }
	}
	
	
}
