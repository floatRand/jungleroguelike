﻿
using System;

namespace Roguelike_Project
{

		public class MONS_DeathHornet : CreatureBase
		{
			public MONS_DeathHornet(int x0, int y0) : base()
			{
				glyph = new Glyph ( 500, 'y', new Colour(255,0,0),  
				                   new Colour(0,0,0,0), false, 1000  );
				name = "Death Hornet"; raceName = "Death Hornet"; raceNamePlural = "Death Hornets";
				maxHP = 50; maxEP = 10; HD = 8; 
				moveSpeed = 4;
				XP_on_kill = CalculateXP();
				spawnFinalize(x0,y0);
				gender = RandomMaleorFemale();
			}
		}
		
		public class MONS_Lunarian : CreatureBase
		{
			public MONS_Lunarian(int x0, int y0) : base()
			{
				glyph = new Glyph ( 500, (char)154, new Colour(255,255,255),
				                   new Colour(0,0,0,0), false, 1000  );
				name = "Lunarian"; raceName = "Lunarian"; raceNamePlural = "Lunarians";
				maxHP = 120; maxEP = 120; HD = 12; 
				moveSpeed = 10;
				XP_on_kill = CalculateXP();
				spawnFinalize(x0,y0);
			}
		}
		
		public class MONS_RandomHallucination : CreatureBase
		{
			public MONS_RandomHallucination(int x0, int y0) : base()
			{
				glyph = new Glyph ( 500, (char)RNG.GetValue(50,180), Colour.ByName(ColourName.Random),
				                   new Colour(0,0,0,0), false, 1000  );
				name = Desciptors.GenerateRandomSillyName(); raceName = name; raceNamePlural = name+"s";
				maxHP = 9999; maxEP = 9999; HD = RNG.GetValue(1,100);
				x = x0; y = y0; moveSpeed = 10;
				XP_on_kill = 0;
				currentHP = RNG.GetValue(1,9999);
			}
		}
		
		
		
		
	
}
