﻿
using System;

namespace Roguelike_Project
{
	/// Curses
	// So far ideas are:
	// Binding ( good ol' sticky curse ) 
	public class ItemEgoBase : IEquatable<ItemEgoBase>
	{
		public bool curse, visibleEgo, identified, obviousEffect;
		public ItemBase masterItem;
		public bool worn_effect, carried_effect, powerDependant;
		public int order; // how they are ordered. In rule of thumb, beneficial things should have NEGATIVE ONE, while nonbeneficial should get 0!
		public int score; // negative for curses
		public int power; // how strong the change is.
		public string Name, shortName, description, equipMessage, unequipMessage;
	
		public ItemEgoBase(ItemBase masr)
		{ 
			masterItem = masr;
			identified = false; visibleEgo = false; curse = false;
			order = 0; score = 0; power = 0; Name = ""; shortName = ""; description = ""; powerDependant = false;
			equipMessage = ""; unequipMessage = "";
			worn_effect = false; carried_effect = false;
		}
		public bool Equals(ItemEgoBase other){ if(shortName==other.shortName) return true; else return false; }
		
		public virtual void Equip_Effect(CreatureBase holder){}
		public virtual void Worn_Effect(CreatureBase holder){}
		public virtual void Carried_Effect(CreatureBase holder){}
		public virtual void Unequip_Effect(CreatureBase holder){}
		
	}
	
	
	public class EGO_test : ItemEgoBase
	{
		public EGO_test(ItemBase masr) : base(masr)
		{ 
			identified = true; visibleEgo = true;  worn_effect = true;
			Name = "Test Ego"; shortName = "TEST"; description = "It has been branded as test item. Tough luck.";
			equipMessage = masterItem.baseName+" is cursed oh noes!";
			unequipMessage = "It wasn't cursed?!??! at least unqeuippung wrks.";
		}
		
		public override void Worn_Effect(CreatureBase holder)
		{	
			MSGH.Print(masterItem.baseName+" does something?!");
			holder.AlterAbilityScore(AbilityScore.strength,10);
			holder.AlterAbilityScore(AbilityScore.will,10);
			holder.AlterAbilityScore(AbilityScore.dexterity,-5);
			if(holder.currentHP<holder.maxHP) holder.currentHP++;
		}
	
	}

	
	
	
	
}
