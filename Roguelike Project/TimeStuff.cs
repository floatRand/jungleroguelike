﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of TimeStuff.
	/// </summary>
	public class GTime{
		public double TURN;
		public double actionNum; 
	
		public static readonly int Conversionrate_turn_to_Min = 20;
		public static readonly int Conversionrate_turn_to_Hour = 1200;
		public static readonly int Conversionrate_turn_to_Day =  28800;
		public static readonly int Conversionrate_turn_to_Week = 201600;
		public static readonly int Conversionrate_turn_to_Month = 864000;
		public static readonly int Conversionrate_turn_to_Year = Conversionrate_turn_to_Month * 12;
		
		public GTime(){ TURN = 0; actionNum = 0; }
		
		public void PlayerActed(){ actionNum++; }
		public void Increment(){ 
			if(TURN<double.MaxValue-1) TURN+=0.1f; // avoid overlow
		}
		public void Reset(){ TURN = 0; actionNum = 0; }
		
		public float GetDayDarkness()
		{
			return Math.Abs((float)Math.Sin( (TURN * (float)Math.PI ) / (double)Conversionrate_turn_to_Day ) ); // 1 = night, 0 = day;
		}
		
		public TimeFormat GetTimeFormat(){ return new TimeFormat( TURN ); }
		
		public static GTime Get(){return gameManager.Instance.time;}
	
	} // gametime, not to conflict with SFML
	
	public struct TimeFormat{
			
		public int year, month, week, day, hour, minute, second;
		public TimeFormat( double turn )
		{
		 // starts at 0
		 // 1 turn = 6 seconds
		 // 10 turn = minute
		 // 600 turns = hour
		 // 14400 = day 
		 // 100800 = week
		 // 432000 = month
		 week = 0;
		 year = (int)Math.Floor( (double)turn / (double)GTime.Conversionrate_turn_to_Year); turn -= year * GTime.Conversionrate_turn_to_Year; 
		 month = (int)Math.Floor( (double)turn / (double)GTime.Conversionrate_turn_to_Month); turn -= month * GTime.Conversionrate_turn_to_Month; 
		 //week = (int)Math.Floor( (double)turn / (double)GTime.Conversionrate_turn_to_Week); turn -= week * GTime.Conversionrate_turn_to_Week;
		 day = (int)Math.Floor( (double)turn / (double)GTime.Conversionrate_turn_to_Day); turn -= day * GTime.Conversionrate_turn_to_Day;
		 hour = (int)Math.Floor( (double)turn / (double)GTime.Conversionrate_turn_to_Hour); turn -= hour * GTime.Conversionrate_turn_to_Hour;
		 minute = (int)Math.Floor( (double)turn / (double)GTime.Conversionrate_turn_to_Min); turn -= minute * GTime.Conversionrate_turn_to_Min;
		 second = (int)((double)turn * 3);
		}

		public string GetDayHours(){
			string shours, sminutes;
			int unhours = hour+12;
			if(unhours>23){ unhours-=23; }
			
			if(unhours<10){ shours = "0"+unhours.ToString(); } else{ shours = unhours.ToString(); }
			if(minute<10){ sminutes = "0"+minute.ToString(); } else{ sminutes = minute.ToString(); }
			return shours+":"+sminutes;
		}
		
		public string GetString()
		{
			string res = "";
			if(year>0) res += "Year "+year+", ";
			if(year>0 || month>0) res += "Month "+month+", ";
			res += "Day "+day;
		
			return res;
		}
		
	}
	
	public static class TimeStuff
	{
		
	}
}
