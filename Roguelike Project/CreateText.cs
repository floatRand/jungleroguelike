﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of CreateText.
	/// </summary>
	public static class CreateText
	{
		public static int[] AddText( this GlyphRenderer rend, string Text, int x, int y, ColourName colourName = ColourName.GUIDefault, ColourName bgCol = ColourName.Black )
		{
			const int maxLength=100;
			int line = 0; int charinline = 0;
			for(int i=0; i<Text.Length; i++)
			{
				if(Text[i] != '¤'){
					rend.InsertGlyph(x+charinline,y+line, new Glyph(0,Text[i],Colour.ByName(colourName),Colour.ByName(bgCol)));
					if(charinline>=maxLength){line++; charinline=0;}
					charinline++;
				}
				else{line++; charinline=0;} 
			}
			return new int[]{ charinline,line };

		}
		
		public static int[] AddText( this GlyphRenderer rend, string Text, int x, int y, Colour colour, Colour bgCol)
		{
			const int maxLength=100;
			int line = 0; int charinline = 0;
			for(int i=0; i<Text.Length; i++)
			{
				if(Text[i] != '¤'){
					rend.InsertGlyph(x+charinline,y+line, new Glyph(0,Text[i],colour,bgCol));
					if(charinline>=maxLength){line++; charinline=0;}
					charinline++;
				}
				else{line++; charinline=0;} 
			}
			return new int[]{ charinline,line };

		}
		
	}
}
