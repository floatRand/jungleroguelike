﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Inventory.
	/// </summary>
	public class Inventory
	{
		public List<Inv_Slot> slots;
		public int slotLimit, page;
		public float totalMass; // kilograms again
		
		public Inventory(int slotLim)
		{
			slots = new List<Inv_Slot>();
			page = 0; // related to browsing...
			slotLimit = slotLim; totalMass = 0;
		}

		public float CalculateMass()
		{
			totalMass = 0;
			foreach(Inv_Slot elm in slots)
			{
				totalMass+=elm.sumMass();
			}
			return totalMass;
		}
		
		public void AddItem(ItemBase it){ 
			if(slots.Exists(elm=>elm.stackName==it.name) && it.stackable) slots.Find(elm=>elm.stackName==it.name).items.Add(it); // there's already one, just increment stack
			else slots.Add( new Inv_Slot(it, this) ); // add new stack for items
			AssignKeys();
		}
		
		public bool HasItem(ItemBase it){ 
			if(slots.Exists(elm=>elm.stackName==it.name)) return true;
			return false;
		}
		public bool HasSpace(){ if(slots.Count<slotLimit) return true; return false; }
		public void CleanUpEmpty(){ slots.RemoveAll(elm=>elm.empty); AssignKeys(); }
		public void AssignKeys()
		{
			for(int i=0;i<slots.Count;i++){
				slots[i].key = hotkeyGen.GetKeyByInt(i);
			}
		}
		public void InvokeCarryEffects(CreatureBase holder)
		{
			foreach(Inv_Slot slot in slots){ slot.GetSample().Carried_Effect(holder); }
		}
	
		
		
	}
	
	public class Inv_Slot : IEquatable<Inv_Slot>
	{
		public List<ItemBase> items;
		public string stackName, IDENT;
		public int stackNum;
		public bool empty, stackable;
		public Inventory master;
		public char key;
		
		public Inv_Slot(ItemBase it, Inventory parent){
			items = new List<ItemBase>(); 
			items.Add(it);
			IDENT = it.IDENT;
			stackNum = 1; stackable = it.stackable;
			empty = false; stackName = it.name;
			key = 'a'; master = parent;
		}
		
		public void SetItem(ItemBase it, Inventory parent){
			items = new List<ItemBase>(); 
			items.Add(it);
			stackNum = 1; stackable = it.stackable;
			empty = false; stackName = it.name;
			key = 'a'; master = parent;
		}
		
		public ItemBase TakeItem()
		{
			ItemBase returnable;
			if(items.Count==0){ returnable = GetSample(); NukeSlot(); master.CleanUpEmpty(); return returnable; }
			else{ returnable=GetSample(); items.Remove(returnable); if(items.Count == 0){ NukeSlot(); master.CleanUpEmpty();}  return returnable; }
		}
		
		public List<ItemBase> TakeMultItem()
		{
			List<ItemBase> returned = new List<ItemBase>();
			int num = Prompts.IntPrompt("How many?");
			if(num>items.Count) num = items.Count;
			for(int i=0; i<num; i++)
			{
				if(items.Count>0){ 
					returned.Add(TakeItem());
				}
			}
			
			if(items.Count==0){ NukeSlot(); master.CleanUpEmpty(); return returned; }
			return returned;
		}
		
		public void NukeSlot(){
			items.Clear();
			stackName = ""; IDENT = "";
			stackNum = 0; stackable = false; empty = true; 
		}
		
		public bool TransferItemTo( Inventory destination ){
			if(destination.HasSpace()){ 
					ItemBase tempItem = items[items.Count - 1]; // the LAST one
					destination.AddItem(tempItem); items.Remove(tempItem);
					if(items.Count<=0){ CleanStack(); }
				return true; // succesful!
			}
			return false; // not succesful!
		}
		public void TransferItemTo( List<ItemBase> destination, bool onGround ) // transfer to map etc.
		{
			ItemBase tempItem = items[items.Count - 1]; // the LAST one
			tempItem.onGround = onGround;
			destination.Add(tempItem); items.Remove(tempItem);
			if(items.Count<=0){ CleanStack(); }
		}
		public float sumMass()
		{
			float mass = 0;
			foreach(ItemBase elm in items){ mass+=elm.mass; }
			return mass;
		}
		public ItemBase GetSample()
		{
			for(int i=0;i<items.Count;i++){
				if(items[0]!=null) return items[0];
			}
			Console.WriteLine("ERROR, NO SAMPLEABLE ITEMS IN "+stackName);
			return null;
		}
		public void CleanStack(){empty = true;}
		public bool Equals(Inv_Slot other){ if(stackName==other.stackName) return true; return false; }
		
	}
	
	
}
