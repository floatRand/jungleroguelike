﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class ShadowMap
	{
			public List<Light> lights;
			public byte[,] values;
			public byte lightValue;
			public ShadowMap(uint width, uint height, byte lightva)
			{
				values = new byte[width,height];
				lights = new List<Light>();
				Initialize(lightva);
				lightValue = lightva;
			}
			
			public void Initialize(byte lightvalue)
			{
				for(int i=0;i<values.GetLength(0);i++){for(int j=0;j<values.GetLength(1);j++){
						values[i,j] = lightvalue;
					}}
			}
			
			public void ApplyLights(Map map, float lightmod = 1.0f)
			{
				foreach(Light L in lights)
				{
					L.Illuminate(map); Console.WriteLine("ApPlie");
				}
				
			}
			
			public void AddLight( int x0, int y0, int rad, bool flicker = false, bool sh = false, bool beamd = false )
			{
				Light tempL = new Light(x0-rad-1,y0-rad-1,rad);
				tempL.flickering = flicker;
				tempL.sharp = sh;
				tempL.beamdown = beamd;
				lights.Add( tempL );
			}
			
			public void ClearLights(){ lights.Clear(); Initialize(lightValue);  }
			
	}
	
	
	public struct Light
	{
		public int x,y;
		public bool flickering,       // animation like torch
						 sharp,       // no half-dark zone
						 beamdown;    // downwards beam, not blocked by wahtever
		public int radius, power;     // ~REGARDING POWER, if we have effects that rid lights, I figure that 
								      // having power-value should be there, so it can resist them? It should have
								      // default value most of the time anyway.
		//protected byte[,] shape;      // usually automatically generated. Non-round shapes only for beamdowns.
									  // 50 <= NO LIGHT, 51...199 = HALF-LIGHT, >205 = FULL LIGHT
									  
		 public Light( int x0, int y0, int rad )
		 {
		 	x = x0; y = y0; radius = rad;
			flickering = false; sharp = false; beamdown = false;
			power = radius; 
		 }
									  
	     public void Illuminate(Map map)
		 {
		 for (int i = -(int)radius; i <= radius; i++){ //iterate out of map bounds as well
    		for (int j = -(int)radius; j <= radius; j++){
			  if(i * i + j * j < radius * radius) Calculate(x, y, i, j, map);
    			}
    		}
		  }
	     
	     private void Calculate(int x0, int y0, int x1, int y1, Map map) 
	     {
   	 		int sx, sy, xnext, ynext, dx, dy;
   			 double denom;
   			 x1 = x0+x1; y1 = y0+y1;
   			 dx = x1 - x0;
   			 dy = y1 - y0;
   	 		if (x0 < x1) sx = 1; else sx = -1;
   	 		if (y0 < y1) sy = 1; else sy = -1;
  		 	 xnext = x0; ynext = y0;
   	     	 denom = Math.Sqrt(dx * dx + dy * dy);
   			 while (xnext != x1 || ynext != y1)
   			 {
     			   // check map bounds here if needed
       			 if(xnext>=0 && xnext<map.shadowMap.values.GetLength(0) &&  ynext>=0 && ynext<map.shadowMap.values.GetLength(1) ){
        		 if (map.mapdata[xnext,ynext].solidity == TileSolidity.solid){return;}
     	 	  		}
       		 // Line-to-point distance formula < 0.5
       		 if(Math.Abs(dy * (xnext - x0 + sx) - dx * (ynext - y0)) / denom < 0.5f) xnext += sx;
        	 else if(Math.Abs(dy * (xnext - x0) - dx * (ynext - y0 + sy)) / denom < 0.5f) ynext += sy;
             else { xnext += sx; ynext += sy; }
   			 }
   	 		 if(x1>=0 && x1<map.shadowMap.values.GetLength(0) &&  y1>=0 && y1<map.shadowMap.values.GetLength(1) ){ 
   	 	  	map.shadowMap.values[x1,y1] = 255;
   	     	if(map.IsCreatureOnTile(x1,y1)){
   	 		//map.GetCreatureOnTile(x1,y1).inFOV = true;
   	 		}
   		 }
		}
	     
	     
	     
	     
	     
									  
									  
	
	}
}
