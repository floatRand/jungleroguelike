﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{

	public enum Gas_SpaceState{ Open, DifferentGas, SameGas, Enqueued, Solid } // closed = filled with solid tile
	
	public class GasCloud
	{
		
		public List<GasNode> gasNodes, encqueuedNodes;
		public bool inheuxtaubleSource, permanent, cloudDissipated;
		public int timeLeft, maxNodes, dissipationThreshold, spreadRate;
		public GasType theGasType;
		public Map habitedMap;
		
		public GasCloud(Map map, int x, int y, int initialD, GasType gasType, int mxlife = 333, int maxN = 700, bool perm = false, bool inexh = false)
		{
			gasNodes = new List<GasNode>();
			encqueuedNodes = new List<GasNode>();
			habitedMap = map;
			theGasType = gasType;
			cloudDissipated = false;
			permanent = perm; timeLeft = mxlife; maxNodes = maxN; spreadRate = theGasType.spreadRate;
			dissipationThreshold = 10;
			GasNode firstNode = new GasNode( this, x, y, initialD, mxlife );
			firstNode.master = true; firstNode.inexhaustable = inexh;
			gasNodes.Add( firstNode );
			
		}
		
		public void UpdateGas()
		{
			int debugCount = 0;
			foreach(GasNode node in gasNodes)
			{
				node.containsObject = false;
				habitedMap.mapdata[node.x,node.y].containsGas = true;
				if(RNG.OneChanceIn(5) || node.concentration > 400) Spread(node);
					
					if(habitedMap.IsCreatureOnTile( node.x, node.y ))
					{
						CreatureBase theCreature = habitedMap.GetCreatureOnTile(node.x,node.y);
						theGasType.InflictEffect( theCreature, node.concentration );
						node.containsObject = true;
					}
					
					if(gameManager.Instance.player.x == node.x && gameManager.Instance.player.y == node.y)
					{
						theGasType.InflictEffect( gameManager.Instance.player, node.concentration );
					    MSGH.Print(theGasType.afflictionMessage);
						node.containsObject = true;
					}
				
				
					if(theGasType.affectsItems){
					if(habitedMap.IsItemOnTile(node.x, node.y))
					{
						ItemBase theItem = habitedMap.GetItemOnTile(node.x,node.y);
						theGasType.ItemEffect( theItem, node.concentration );
						node.containsObject = true;
					
					}}
					
					if(!permanent) node.lifeLeft--;
				if(node.lifeLeft<=0 && RNG.OneChanceIn(2) ){ node.dissipated = true; }
				if(node.concentration<dissipationThreshold && RNG.OneChanceIn(4) ){ node.dissipated = true; }
				if(node.concentration<=0 ){ node.dissipated = true; }
				debugCount++;
			}
			
			CreateNewNodes();
			
			gasNodes.RemoveAll( elm => elm.dissipated ); // remove dead nodes.
			if( gasNodes.Count + encqueuedNodes.Count == 0 ){ cloudDissipated = true; Console.Write("Cloud dissipated");} // kill the cloud.
			
		}
		
		private void CreateNewNodes()
		{
			gasNodes.AddRange(encqueuedNodes);
			encqueuedNodes.Clear();
		}
		
		private void Spread(GasNode spreadNode)
		{
			List<int[]> habitablePoints = GetOpenAdjancentTiles( spreadNode );
			List<GasNode> friendNodes = new List<GasNode>();
			int concSum = 0;
			foreach(int[] hP in habitablePoints)
			{
				if(CheckForOpenSpace(hP[0], hP[1]) == Gas_SpaceState.SameGas){ 
					GasNode otherNode = GetGasNodeInLocation(hP[0], hP[1]);
					if(otherNode!=null){ friendNodes.Add( otherNode ); concSum+=otherNode.concentration; } // try to
				} // if it is same gas, 
			}
			
				int numNodes = friendNodes.Count; int allSpaces = habitablePoints.Count;
				int openSpaces = allSpaces-numNodes;
				
				
				int spreadDens = 0;
				if(openSpaces>0){ spreadDens=(int)((float)spreadNode.concentration/(1.5f) ) / openSpaces; 
					if(spreadDens<1) spreadDens = 1; if(spreadDens>1500) spreadDens =1500;}
				else{ spreadDens = 0; }
				
				foreach(int[] hP in habitablePoints)
				{
				if(CheckForOpenSpace(hP[0], hP[1]) == Gas_SpaceState.Open 
					   && gasNodes.Count<=maxNodes && spreadNode.concentration > 0 && spreadDens>0
					  ){ encqueuedNodes.Add(new GasNode(this,hP[0],hP[1],spreadDens, timeLeft )); spreadNode.concentration-=spreadDens;  } // create a new node!
				}
				
				foreach( GasNode otherNode in friendNodes )
				{
					if( otherNode.concentration>spreadNode.concentration && RNG.Coinflip() ){
						otherNode.concentration-=10; spreadNode.concentration+=10;
					}
					else if( otherNode.concentration<spreadNode.concentration  && RNG.Coinflip() ){
						otherNode.concentration+=10; spreadNode.concentration-=10;
					}
				}
				
			spreadNode.concentration-=1;
				
			
		}
		
		private Gas_SpaceState CheckForOpenSpace(int x, int y)
		{
			if( x>=0 && x<habitedMap.mapdata.GetLength(0) && y>=0 && y<habitedMap.mapdata.GetLength(1) )
			{
				if(habitedMap.mapdata[x,y].containsGas){ 
					if( HabitingTile(x,y) ) return Gas_SpaceState.SameGas;
					return Gas_SpaceState.DifferentGas;
				}
				if(encqueuedNodes.Exists(elm=>elm.x==x && elm.y==y)) return Gas_SpaceState.Enqueued;
				if(habitedMap.mapdata[x,y].solidity==TileSolidity.fluff || habitedMap.mapdata[x,y].solidity==TileSolidity.none) 
					return Gas_SpaceState.Open;
				
			}
			return Gas_SpaceState.Solid;
		}
		
		private bool HabitingTile(int x, int y)
		{
			foreach( GasNode node in gasNodes ){ if(node.x == x && node.y == y) return true; }
			return false;
		}
		
		private List<int[]> GetOpenAdjancentTiles(GasNode centerNode){
			List<int[]> returnedTiles = new List<int[]>();
			foreach( int[] loc in Pathfinding.moveDirs ){
				if( CheckForOpenSpace( centerNode.x+loc[0],centerNode.y+loc[1] ) != Gas_SpaceState.Solid ) 
					returnedTiles.Add( new int[]{centerNode.x+loc[0],centerNode.y+loc[1]} );
			}
			return returnedTiles;
		}
		
		public GasNode GetGasNodeInLocation(int x, int y)
		{
			if(gasNodes.Exists( elm => ( elm.x == x && elm.y == y ))) return gasNodes.Find( elm => ( elm.x == x && elm.y == y ));
			else return null;
		}
		
		public void DrawGas( GlyphRenderer renderer )
		{
			foreach( GasNode node in gasNodes )
			{
				if(!node.containsObject || node.concentration>=theGasType.blockVisibilityThreshold)
				{
					if(habitedMap.mapdata[node.x,node.y].seen) renderer.InsertGlyph( node.x, node.y, GetGasGlyph( node.concentration ), true );
				}
			}
		}
		
		private Glyph GetGasGlyph(int C){  
			if(C>18) return new Glyph( 0, (char)219, theGasType.colour, new Colour(0,0,0,0)  );
			if(C>6) return new Glyph( 0, (char)178, theGasType.colour, new Colour(0,0,0,0)  );
			if(C>2) return new Glyph( 0, (char)177, theGasType.colour, new Colour(0,0,0,0) );
			return new Glyph( 0, (char)176, theGasType.colour, new Colour(0,0,0,0)  );
		
		}
		
		
	}

	public class GasNode
	{
		public bool master, blockSight, inexhaustable, dissipated, containsObject;
		public int concentration, lifeLeft, x, y; 
		public GasCloud containedIn;
		
		public GasNode(GasCloud parent, int x0, int y0, int D, int life)
		{
			containedIn = parent; x = x0; y = y0; concentration = D;
			lifeLeft = life; master = false; blockSight = false; dissipated = false; containsObject = false;
		}
		
	}


	public class GasType{
		public string name, afflictionMessage;
		public bool visible, smells, felt, affectsItems; // what sort of message do we get / is gas drawn? 
		public Colour colour;
		public int blockVisibilityThreshold, spreadRate; // spreadrate should be 1 
		
		public virtual void InflictEffect(CreatureBase target, int pressure){} // effect for creatures, check for breathing etc. in overrides
		public virtual void ItemEffect(ItemBase target, int pressure){} // for items, for example acid clouds could degrade/destroy equipment
		public virtual void MixReaction(GasType other){} // mixture reaction
		public virtual void BurnReaction(int pressure){}
	}
	
	public class GAS_Test : GasType
	{
		public GAS_Test()
		{
			name = "Buggy Gas";
			afflictionMessage = "You are enqulfed in buggy gas.";
			visible = true; smells = true; felt = true; affectsItems = false;
			colour = new Colour(155,255,255);
			blockVisibilityThreshold = 80; spreadRate = 1;
		}
		
		public override void InflictEffect(CreatureBase target, int pressure)
		{
			target.currentHP--;
			if(target.IsPlayer){ MSGH.Print("Current concentration is "+pressure); }
		}
	
	}
	
	
}
