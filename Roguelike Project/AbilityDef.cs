﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of AbilityDef.
	/// </summary>
	public class TestAbilityHeal : Ability
	{
		public TestAbilityHeal()
		{
			name = "Heal Self";
			fullName = "Healing Surge";
			shortDescription = "Heal self at cost of nutrition"; 
			description = "Heal self at cost of nutrition, you pussy";
			actionDescription = "Healing itself";
			gainText = "You can heal yourself"; loseText = "You can no longer heal yourself";
			unlockatHD = 1;
			energyCost = 20; energypostCost = 10;
			abilityType = AbilityType.Natural;
			key = 'a';
		}
		
		public override int ChanceofFailure(CreatureBase user)
			{
				return 30 - user.HD;
			}
		
		public override void TheAbilityMethod(CreatureBase user, CreatureBase target)
		{
			user.currentHP = user.currentHP + CalculatePower(user)/10;
		}
		
		
	}
}
