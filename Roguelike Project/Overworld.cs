﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	/**
	 *  Okay, this is awkward. I am gonna make the two systems ( map and this ) different, since I figure they will contain
	 *  different sort of things. Tiles will have encounter rates, etc. It might have been best to do as inheriting from map, but this
	 *  is probably bit easier method so far. It means I need to implement LOS for this as well. Although it should be really easy overall.
	 *  But anyway.
	 * */
	public enum OverWorldType{ Jungle, Underground, Sheol }
	public enum Terrain{	
						 // These are for setupping certain generation-rates and encounter-rates. Plus they are biggest definers for terrain generation!
						 // Generic
						 Inaccessible,
						 Nothing, 
						 Special, // has something ( permanent dungeon entrance etc. )
						 // These are terrains of the jungle-overworld. Basically regular world!
						 Desert, // borders the forest.
						 Plain, // a plain plain
						 Forest,
						 Jungle,
						 Rainforest, // blocks out light. Actually good place for the
						 UltraDenseForest, // cannot be traversed without
						 ForestCore, // incredible dangerous, the final area
						 ForestRuins,
						 Wasteland,
						 Deadlands, // undead playhouse
						 Mountain, // cliffs etc.
						 River,
						 Lake, // these just might be useful
						 Sea,
						 // Underground... Devious passages and lost cities!
						 RoughStoneWall, // eyy
						 CraftedStoneWall, // Constructed cavern wall, to differientate from regular cavern wall
						 Cavern, // regular cavern
						 CityRuins, // ruins of zere
						 Pit, // can only be traversed with flying!
						 // Sheol. Dark, otherworldy wasteland, with great citadel of Pandemonium!
						 SheoleanPlain,
						 SheoleanCavern,
						 SheoleanWall,
						 PandemoniumWall,
						 PandemoniumTerrace,
						 Abyss,
						 ////////////////////////////
						 other // what the hell is this
	}
	
	public class Overworld
	{
		
		public WorldTile[,] mapdata;
		// regarding the size of the world. I was figuring that one tile translates to about 5 kilometers.
		// Base the traversing time on that...
		// So, in generally unobstructed terrain, player advances about 5km / h, thus traveling player can travel 24 tiles in a day.
		// so the forest also spans over thousand of kilometers...
		// It takes more than week to travel through. Difficult, difficult. This will probably be subject to changes over time
		// Weahter effects need to be in place too, rainforests etc. are known to be rather rainy.
		
		// Oh yeah, important. In forest, the levels are not persistent. This is because it is a 'living' forest. It also makes things bit more interesting.
		// You cannot get to some special places since navigation is so devilishly difficult. There is item for marking places, though!
		// TODO: Visual effects, FOV, clouds ( shadows of them )!
		public const uint sizeX = 200, sizeY = 200;
		public Glyph[,] glyphMap;
		public ShadowMap shadowMap;
		public int playerPosX, playerPosY;
		
		public Overworld()
		{
			mapdata = new WorldTile[sizeX,sizeY];
			InitWorld(Terrain.Desert);
		    GenerateForest();  // slow, let's not have it for now
			ExtractGlyphMap();
			playerPosX = 0; playerPosY = 0;
			shadowMap = new ShadowMap(sizeX,sizeY,255);
		}
		
		public void ExtractGlyphMap(){
			glyphMap = new Glyph[mapdata.GetLength(0),mapdata.GetLength(1)];
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					glyphMap[i,j] = mapdata[i,j].glyph;
				}
			}
		}
		
		public void SetMemorizedFeatures(Glyph[,] glmap, int x0, int y0) // uses same thing...
		{
			for(int i = 0; i<glmap.GetLength(0); i++ ){
				for (int j = 0; j<glmap.GetLength(1); j++){
					if(i+x0>=0 && i+x0<mapdata.GetLength(0) && j+y0>=0 && j+y0<mapdata.GetLength(1) )
					{
						if(mapdata[i+x0,j+y0].tileMemory.memorized // memorized
						   && !mapdata[i+x0, j+y0].seen // and not directly seen
						  ) { glmap[i,j] = mapdata[i+x0,j+y0].tileMemory.glyph; }
					}
				}
			}
		}
		
		// Characters at beginning have choice of purchasing a map. Either way, important dungeons are always mapped.
		// MagicMapping creates PERFECT mapping of jungle. It doesn't reveal -details- like location of the final boss, etc.
		// Fuzzymapping mixes up things initially. All forest-tiles are same etc. It is just a BIG blob of forest.
		// Obviously, accuratemap costs more, but makes traversal bit more easier since dense forest is slower to travel.
		public void MagicMapping()
		{
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					mapdata[i,j].tileMemory.SetMemory(mapdata[i,j].glyph);
				}
			}
		}
		
		public void FuzzyMapping()
		{ // TODO: fuzzymappng
		
		}
		
		public void RandomGenerate()
		{
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					mapdata[i,j] = new WorldTile( RandomJungleTerrain() );
				}
			}
		}
		
		public void GenerateForest()
		{ // TEST
			
			List<int[]> startNodes = new List<int[]>();
			for(int a=0;a<3;a++){startNodes.Add(new int[]{
				(int)(sizeX/2+RNG.GetValue(-(int)(sizeX/5),(int)(sizeX/5))),
				(int)(sizeY/2+RNG.GetValue(-(int)(sizeY/5),(int)(sizeY/5)))});
			}
			
			int[,] rustleMap = new int[sizeX,sizeY];
			ZeroRustle(rustleMap);
			SetTerrainFromNumber( rustleMap, Terrain.Desert, 0);
			// Let's have some plains
			ZeroRustle(rustleMap);
			MakeBlobby(rustleMap, new int[]{ RNG.GetValue(5,rustleMap.GetLength(0)-5),1 }, 300 );
			DissolveRustle(rustleMap,7);
			SetTerrainFromNumber( rustleMap, Terrain.Plain, 1);
			
			//Base forest
			foreach( int[] initialNode in startNodes ){ MakeBlobby( rustleMap,initialNode ); }
			DissolveRustle(rustleMap,5);
			SetTerrainFromNumber( rustleMap, Terrain.Forest, 1);
			// Jungle
			foreach( int[] initialNode in startNodes ){ MakeBlobby( rustleMap,initialNode,100 ); }
			DissolveRustle(rustleMap,5);
			SetTerrainFromNumber( rustleMap, Terrain.Jungle, 1);
			
			int[] centralNode = new int[2]{0,0};
			foreach( int[] initialNode in startNodes ){ centralNode[0]+=initialNode[0]; centralNode[1]+=initialNode[1]; }
			centralNode[0] = centralNode[0] / startNodes.Count;
			centralNode[1] = centralNode[1] / startNodes.Count;
			
			foreach( int[] initialNode in startNodes ){ mapdata[initialNode[0],initialNode[1]] = new WorldTile(Terrain.Abyss); }
			//Rainforest
			ZeroRustle(rustleMap);
			MakeBlobby(rustleMap, centralNode, 30 );
			DissolveRustle(rustleMap,5);
			SetTerrainFromNumber( rustleMap, Terrain.Rainforest, 1);
	
			//Core
			ZeroRustle(rustleMap);
			MakeBlobby(rustleMap, centralNode, 4, 0, 500 );
			DissolveRustle(rustleMap,5);
			SetTerrainFromNumber( rustleMap, Terrain.ForestCore, 1);
			//CTestcrescent
			ZeroRustle(rustleMap);
			MakeBlobby(rustleMap, centralNode, 1, 10, 30 );
			DissolveRustle(rustleMap,4);
			SetTerrainFromNumber( rustleMap, Terrain.UltraDenseForest, 1);
			//Mountain1
			for(int i=0;i<3;i++){
			ZeroRustle(rustleMap);
			int nega = 1; if(RNG.Coinflip()) nega = -1; int nega2 = 1; if(RNG.Coinflip()) nega2 = -1;
			MakeBlobby(rustleMap, new int[]{centralNode[0]+RNG.GetValue(20,50)*nega, centralNode[1]+RNG.GetValue(20,50)*nega2   }, RNG.GetValue(1,2), RNG.GetValue(10,70) );
			DissolveRustle(rustleMap,5);
			SetTerrainFromNumber( rustleMap, Terrain.Mountain, 1);
			}
			
			
	
	
		
		}
		
		private void InitWorld(Terrain terra){
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					mapdata[i,j] = new WorldTile(terra);
				}
			}
		}
		
		private void ZeroRustle(int[,] rustleMap){ 
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					rustleMap[i,j] = 0;
				}
			}
		}
		
		private void SetTerrainFromNumber(int[,] rustleMap, Terrain terra, int matchNum)
		{
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					if(rustleMap[i,j] == 1) mapdata[i,j] = new WorldTile(terra);
				}
			}
		}
		
		
		private void MakeBlobby(int[,] rustleMap, int[] initialNode, int divider = 200, int overrideValue = 0, int rThreshold = 0){
			int distanceFromNode = 1000;
			
			divider = (int)(divider * (sizeX/10));
			if(overrideValue!=0){ divider = overrideValue; }
			for(int i = 0; i<mapdata.GetLength(0); i++ ){
				for (int j = 0; j<mapdata.GetLength(1); j++){
					distanceFromNode = (int)Math.Pow(( initialNode[0] - i ),2) + (int)Math.Pow(( initialNode[1] - j ),2);
					
					if(RNG.OneChanceIn(distanceFromNode/divider) && ( rThreshold==0 || distanceFromNode<=rThreshold )) rustleMap[i,j] = 1;
					else rustleMap[i,j] = 0;
					
				}
				}	
		}

		
		private void DissolveRustle(int[,] rustleMap, int iter)
		{
			for(int a = 0; a<iter; a++ ){
			for (int i = 0; i < rustleMap.GetLength(0); i++){
  					  for (int j = 0; j <  rustleMap.GetLength(1); j++){
						if((CountTilesAround(rustleMap,i,j,1)>=4 || 
						    ( CountTilesAround(rustleMap,i,j,1)!=0 && a<4 )) && rustleMap[i,j] == 1){
							rustleMap[i,j] = 1; }
						else if(CountTilesAround(rustleMap,i,j,1)>=5 && rustleMap[i,j] != 1){ 
							rustleMap[i,j] = 1; }
						else{  rustleMap[i,j] = 0; }
					
					}
			}
			}
		
		}
		
		private static int CountTilesAround( int[,] map, int x, int y, int matchID )
		{
			int num = 0;
			int maxY = map.GetLength(1);
			int maxX = map.GetLength(0);
			if(x>0 && y>0){      if(map[x-1,y-1] == matchID ){ num++; }}  // upper left
			if(y>0){             if(map[x,y-1] == matchID ){ num++; }} // up
			if(x>0){             if(map[x-1,y] == matchID ){ num++; }} // left
			if(x<maxX-1 && y>0){   if(map[x+1,y-1] == matchID ){ num++; }} // upper right
			if(x<maxX-1){ 		 if(map[x+1,y]  == matchID ){ num++; }} // right
			if(x<maxX-1 && y<maxY-1){if(map[x+1,y+1] == matchID ){ num++; }} // lower right
			if(y<maxY-1){			 if(map[x,y+1] == matchID ){ num++; }} // down
			if(x>0 && y<maxY-1){   if(map[x-1,y+1]  == matchID ){ num++; }} // lower left
		    return num;
		}
		
		
		public static Terrain RandomJungleTerrain()
		{
			int rnd = RNG.GetValue(0,11);
			switch(rnd)
			{
					case 0: return Terrain.Deadlands;
					case 1: return Terrain.Jungle;
					case 2: return Terrain.Forest;
					case 3: return Terrain.Rainforest;
					case 4: return Terrain.ForestCore;
					case 5: return Terrain.Desert;
					case 6: return Terrain.Plain;
					case 7: return Terrain.Mountain;
					case 8: return Terrain.Forest;
					case 9: return Terrain.Jungle;
					case 10: return Terrain.Rainforest;
					case 11: return Terrain.ForestRuins;		
					default: return Terrain.Nothing;
			}
			
		}
		
		
	}
	
	public class WorldTile
	{
		public bool breakable; // I kinda want to keep this. Maybe some monster could have spell so mighty that it alters the world map!
		public bool seen, containsGas, transparent; // these are things that should be kept as well
		public Terrain terrain;
		public Glyph glyph; 
		public GasType gasType; // some tiles can be filled with a gas...
		public TileMemory tileMemory; // this can be used safely with this!
		
		public WorldTile(Terrain terra, bool randomTile = true) // unlike for regular tiles, the terrain defines the tile
		{	
			terrain = terra;
			glyph = GetTerrainGlyph(terrain, randomTile);
			tileMemory = new TileMemory();
			tileMemory.glyph = glyph;
			containsGas = false;
			transparent = true; // so far because goddamn I need to set it up
		}
	
		public char ReturnCharacter(){
			return glyph.character;
		}
		
		public static Glyph GetTerrainGlyph(Terrain terra, bool randomization = true) // randomization is just to add some colour to it.
		{
		
			Colour jungleBG = new Colour(true, 5+RNG.GetValue(0,45),70+RNG.GetValue(0,45),5+RNG.GetValue(0,45));
			Colour darkjungleBG = new Colour(true, 2+RNG.GetValue(0,25),35+RNG.GetValue(0,25),2+RNG.GetValue(0,25));
			switch(terra)
			{ // switch
				case Terrain.CityRuins:
				if(randomization)
					return (Glyph)RNG.SelectRandom(new Glyph[]{
					                     new Glyph(0,'£',new Colour(99,99,99),new Colour(15,15,15)),
					                     new Glyph(0,'%',new Colour(99,99,99),new Colour(15,15,15)),
										 new Glyph(0,'=',new Colour(99,99,99),new Colour(15,15,15)),	
										 new Glyph(0,'&',new Colour(99,99,99),new Colour(15,15,15)),	
					                               }
					);
				return new Glyph(0,'%',new Colour(99,99,99),new Colour(15,15,15));
		
				case Terrain.Forest: return new Glyph(0,(char)131,new Colour(99,255,99),jungleBG);
				case Terrain.Jungle: return new Glyph(0,(char)225,new Colour(99,222,99),jungleBG);
				case Terrain.Rainforest: return new Glyph(0,(char)244,new Colour(99,200,120),jungleBG);
				case Terrain.Deadlands: return new Glyph(0,(char)240,new Colour(5,85,5),new Colour(0,0,0));
				case Terrain.Desert: return new Glyph(0,'~',new Colour(255,255,255),new Colour(200,198,15));
				case Terrain.Plain: return new Glyph(0,'.',new Colour(99,222,99),new Colour(100,188,100));
				case Terrain.ForestCore: return new Glyph(0,(char)234,new Colour(50,155,100),jungleBG); // Omega for the end!
				case Terrain.ForestRuins: return new Glyph(0,'&',new Colour(111,144,111),jungleBG);
				case Terrain.UltraDenseForest: return new Glyph(0,'#',new Colour(50,155,100),darkjungleBG);
				case Terrain.River: return new Glyph(0,'~',new Colour(180,200,255),new Colour(60,120,200));
				case Terrain.Lake: return new Glyph(0,'-',new Colour(180,200,255),new Colour(60,120,200));
				case Terrain.Mountain: return new Glyph(0,(char)030,new Colour(200,200,200),jungleBG);
				case Terrain.Sea: return new Glyph(0,'~',new Colour(120,150,225),new Colour(40,80,150));
				case Terrain.Wasteland: return new Glyph(0,'=',new Colour(170,150,110),new Colour(138,117,72));
				
			} // switch
			return new Glyph(0,'.',Colour.ByName(ColourName.White), Colour.ByName(ColourName.Black));
			
		}
		
	}

	
	
	
}


