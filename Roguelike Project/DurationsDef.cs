﻿
using System;

namespace Roguelike_Project
{
	
	
	public class DUR_Poison : DurationBase
	{
		public DUR_Poison(int dur, int sev, CreatureBase targ, string sourc, bool perm=false) : base(dur,sev,targ,sourc,perm)
		{
			shortName = "PSN";
			Name = "poison";
			description = "poisoned";
			colour = new Colour(255,255,0);
		}
		public override void OnInflict()
		{
			if(target.IsPlayer) MSGH.Print("You feel ill.");
			else MSGH.Print(target.name+" looks ill.");
		}
		public override void Effect()
		{
			if(RNG.Coinflip()) target.currentHP-=RNG.RollDie(1,severity);
			if(target.IsPlayer) MSGH.Print("You feel ill.");
			else MSGH.Print(target.name+" looks ill.");
		}
		public override void OnRemove(bool nonNaturalExperition)
		{
			if(target.IsPlayer) MSGH.Print("You feel better.");
			else MSGH.Print(target.name+" looks healthier.");
		}
		
		public override void OnLengthen(int dur, int sev){
			if(severity<sev) severity = sev;
			duration = duration + dur;
			if(target.IsPlayer) MSGH.Print("You feel even more sick.");
			else MSGH.Print(target.name+" looks sicker.");
		}
	}
	
	public class DUR_Paralyzed : DurationBase
	{
		public DUR_Paralyzed(int dur, int sev, CreatureBase targ, string sourc, bool perm=false) : base(dur,sev,targ,sourc,perm)
		{
			shortName = "PARA";
			Name = "paralyzation";
			description = "paralyzed";
			colour = new Colour(255,0,0);
		}
		public override void OnInflict()
		{
			if(target.IsPlayer) MSGH.Print("You are suddenly collapse!");
			else MSGH.Print(target.name+" suddenly collapses!");
		}
		public override void Effect()
		{
			if(target.IsPlayer) MSGH.Print("You cannot move.");
			target.AlterAbilityScore(AbilityScore.dexterity, -30);
		}
		public override void OnRemove(bool nonNaturalExperition)
		{
			if(target.IsPlayer) MSGH.Print("You recover from paralyzation.");
			else MSGH.Print(target.name+" can move again.");
		}
	}
	
}
