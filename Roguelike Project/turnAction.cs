﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{
	
	public enum ActionState{Finished, Executed, Pending, Aborted}
	
	public class ActionQueueClass
	{
		public List<turnAction> actionList;
		public ActionQueueClass()
		{
			actionList = new List<turnAction>();
		}
		
		public void ProcessQueue()
		{
			foreach( turnAction act in actionList )
			{
				if(act.state!=ActionState.Finished) act.Update();
			}
			CleanUpFinishedturnActions();
		}
		
		
		public void CleanUpFinishedturnActions()
		{
			actionList.RemoveAll( IsFinished );
		}
		
		public void Add(turnAction act)
		{
			actionList.Add(act);
		}
		

		private static bool IsFinished(turnAction act) {if(act.state == ActionState.Finished) return true; return false;}
		
	}
	
	public class turnAction
	{
		public ActionState state;
		public Action payload;
		public int postCost, preCost;
		public bool specialInterrupt;
		public string descripe;
		public turnAction(Action theturnActionMethod, int pCost, int afterCost, string description = "doing something", bool specInterrupt = false)
		{
			payload = delegate{theturnActionMethod();};
			postCost = afterCost; preCost = pCost; specialInterrupt = specInterrupt;
			state = ActionState.Pending;
			descripe = description;
		}
		
		public void Update()
		{
			if(state == ActionState.Pending){ 
				preCost--; 
				if(preCost<=0){ Do();}
			}
			if(state == ActionState.Executed){
				postCost--;
			}
			if(postCost<=0 && preCost<=0 && state == ActionState.Executed){ state = ActionState.Finished;}
			if(state == ActionState.Aborted)
			{
				if(specialInterrupt){ Interrupt(); state = ActionState.Finished; }
				else state = ActionState.Finished;
			}
			
		}
		
		public void Do()
		{
			state = ActionState.Executed;
			payload.Invoke();
		}
		
		public void Interrupt()
		{
			// etc...
		}
		
		
	}
	
}
