﻿
using System;

namespace Roguelike_Project
{
	/// <summary>
	/// Description of MapMemory.
	/// </summary>
	public class TileMemory{
		public Glyph glyph;
		public bool memorized;
		
		public TileMemory(){
			glyph = new Glyph(' '); memorized = false;
		}
		
		public void SetMemory(Glyph newGlyph)
		{
			glyph = newGlyph;
			glyph.charColor = Colour.GrayScaleDarker(glyph.charColor);
			glyph.bgColor = Colour.GrayScaleDarker(glyph.bgColor);
			memorized = true;

		}
		
	}
	
	public class MemorizedFeature{} // ...later 
	
}
