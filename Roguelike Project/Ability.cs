﻿
using System;
using System.Collections.Generic;

namespace Roguelike_Project
{

	public enum AbilityType{ Technique, Natural, Magical, Spell, Invocation, Evocation, Other }
	
	
	public class Ability
	{
		// important: name is ident of the abilit so please don't abuse it
		public string name, fullName, shortDescription, description, actionDescription, gainText, loseText;
		public int unlockatHD;
		public int energyCost, energypostCost;
		public AbilityType abilityType;
		public char key;
		
		public Ability(){}
		public bool UseAbility( CreatureBase user, CreatureBase target )
		{
			// target prompts here and whatnot...
			// Target information should be probably passed in sort of class that can contain varying types of information. Specific array of tiles,
			// positions, multiple targets... At the moment this shall do.
			if(ChanceofFailure(user)>=RNG.GetValue(0,100)) return false;
			user.SetAction(new turnAction( delegate{TheAbilityMethod(user,target);}, energyCost,energypostCost,actionDescription ));
			GenerateMessages(user, target);
			return true;
		}
		
		public virtual void TheAbilityMethod(  CreatureBase user, CreatureBase target  )
		{
			 // magical realm 
		}
		public virtual int ChanceofFailure(CreatureBase user){ return 0; } // 0 - 100%
		public virtual int CalculatePower(CreatureBase user){ return 100; }
		
		public virtual void GenerateMessages( CreatureBase user, CreatureBase target )
		{
			MSGH.Print(user.name+" uses "+fullName+" on "+target.name);
		}
		public virtual void GenerateFailureMessage( CreatureBase user, CreatureBase target )
		{
			MSGH.Print(user.name+" failed to use "+fullName+" on "+target.name);
		}
		
		public bool Equals(Ability other){if(name==other.name) return true; return false;}
		
		
	}
	
	
	public static class AbilityHelpers{
		public static void AssignHotkeystoAbilities(this List<Ability> abilList){for(int i=0; i<abilList.Count; i++){ abilList[i].key = hotkeyGen.GetKeyByInt(i); }}
		public static void LearnAbility(this List<Ability> abilList, Ability abil){
			if(!abilList.Exists(elm => elm.name==abil.name)){
				abilList.Add(abil);
				MSGH.Print(abil.gainText);
				abilList.AssignHotkeystoAbilities();
			}
			else{ MSGH.Print("You already have "+abil.fullName+"."); }
		}
		public static void UnlearnAbility(this List<Ability> abilList, Ability abil){
			if(abilList.Exists(elm => elm.name==abil.name)){
				abilList.RemoveAt(abilList.FindIndex(elm => elm.name==abil.name));
				MSGH.Print(abil.loseText);
				abilList.AssignHotkeystoAbilities();
			}
			else{ MSGH.Print("You don't have "+abil.fullName+"."); }
		}
		
		
		
		
	}
}
